#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($busco_list,$busco_folder,$temp_folder,$transdecoder_folder,$transdecoder_filtered_folder,$blast_folder,$blast_filtered_folder,$phylogeny_names,$fnodes,$fnodes_clean,$usegroups);
my ($contaminant_evalue,$contaminant_max_target_seqs,$nt_file_pre,$gis_file_pre,$out_file_pre,$nt_file_post,$gis_file_post,$out_file_post,$batch_command);

# Read user defined variables
GetOptions (  	"contaminant_evalue=f" => \$contaminant_evalue,
                "contaminant_max_target_seqs=f" => \$contaminant_max_target_seqs,
                "nt_file_pre=s" => \$nt_file_pre,
                "gis_file_pre=s" => \$gis_file_pre,
                "out_file_pre=s" => \$out_file_pre,
				"nt_file_post=s" => \$nt_file_post,
                "gis_file_post=s" => \$gis_file_post,
				"out_file_post=s" => \$out_file_post,
				"busco_list=s" => \$busco_list,
				"busco_folder=s" => \$busco_folder,
				"temp_folder=s" => \$temp_folder,
				"blast_folder=s" => \$blast_folder,
				"blast_filtered_folder=s" => \$blast_filtered_folder,
				"transdecoder_folder=s" => \$transdecoder_folder,
				"transdecoder_filtered_folder=s" => \$transdecoder_filtered_folder,
                "phylogeny_names=s" => \$phylogeny_names,
				"fnodes=s" => \$fnodes,
				"fnodes_clean=s" => \$fnodes_clean,
				"batch_command=s" => \$batch_command,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for adapt-BUSCOS\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$queue_file_name);
my (@a,@b);
my (%names,%new_names,%short_names,%cont,%nodes);


# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

my ($xx,$yy,$zz,@aa,@bb,@cc,%hh1,%hh2,%index_names);

# Concatenate and create blast databases
if (1==1){
	print "Concatenating aminoacid and nucleotide files\n";
	if (-e "$blast_filtered_folder/$sp_num\_concatenated.fa"){system ("rm $blast_filtered_folder/$sp_num\_concatenated.fa");}
	if (-e "$blast_filtered_folder/$sp_num\_concatenated.n.fa"){system ("rm $blast_filtered_folder/$sp_num\_concatenated.n.fa");}

	open (COR,">$blast_filtered_folder/ids_correspondances.txt");

	$z=0;
	foreach $x (sort { lc($a) cmp lc($b)} keys %names){
		if (! -e "$transdecoder_folder/$x.longest_orfs.pep"){print "ERROR: $transdecoder_folder/$x.longest_orfs.pep doesn't exist\n";exit(0);}
		if (! -e "$transdecoder_folder/$x.longest_orfs.cds"){print "ERROR: $transdecoder_folder/$x.longest_orfs.cds doesn't exist\n";exit(0);}
	
		$z++;print "\tDoing $new_names{$x} ($z out of $sp_num) (Concatenating sequence files)\n";


		open (OPEN,"<$transdecoder_folder/$x.longest_orfs.pep");
		open (SAVE,">>$blast_filtered_folder/$sp_num\_concatenated.fa");
		$xx=0;
		while ($y = <OPEN>){
			chomp $y;
			if ($y=~ /^>/){
				$y=~ s/>//;
				@a=split(/\@/,$y);
				if (!$hh1{$a[1]}){
					$xx++;
					$hh1{$a[1]}="$a[0]\@buscos-$xx";
					print SAVE ">$a[0]\@buscos-$xx\n";
					print COR "$y\t$a[0]\@buscos-$xx\n";
				}
				else {
					print "Error $y\n";
				}
			}
			else {
				print SAVE "$y\n";
			}
		}
		close (OPEN);
		close (SAVE);
	
		open (OPEN,"<$transdecoder_folder/$x.longest_orfs.cds");
		open (SAVE,">>$blast_filtered_folder/$sp_num\_concatenated.n.fa");
		while ($y = <OPEN>){
			chomp $y;
			if ($y=~ /^>/){
				$y=~ s/>//;
				@a=split(/\@/,$y);
				if ($hh1{$a[1]}){
					print SAVE ">$hh1{$a[1]}\n";
				}
				else {
					print "Error $y\n";
				}
			}
			else {
				print SAVE "$y\n";
			}
		}
		close (OPEN);
		close (SAVE);
	}
	close (COR);
	print "Formatting blastdb (protein)\n";
	system ("makeblastdb -in $blast_filtered_folder/$sp_num\_concatenated.fa -parse_seqids -dbtype prot");
	print "Formatting blastdb (nucleotide)\n";      # We'll need it much later to build nucleotide alignment from protein alignments
	system ("makeblastdb -in $blast_filtered_folder/$sp_num\_concatenated.n.fa -parse_seqids -dbtype nucl");
}

if (1==1){
	%hh1=();
	open (OPEN,"<$blast_filtered_folder/ids_correspondances.txt");
	while ($y = <OPEN>){
		chomp $y;
		@a=split(/\s+/,$y);
		$hh1{$a[0]}=$a[1];
	}
	close (OPEN);

	open (OPEN,"<$fnodes_clean.fullnames");
	open (SAVE,">$fnodes_clean");
	while ($y = <OPEN>){
		chomp $y;
		@a=split(/ -> /,$y);
		@b=split(/ /,$a[1]);
		print SAVE "$a[0] ->";
		foreach (@b){
			print SAVE " $hh1{$_}";
		}
		print SAVE "\n";
	}
	close (OPEN);
	close (SAVE);
}
