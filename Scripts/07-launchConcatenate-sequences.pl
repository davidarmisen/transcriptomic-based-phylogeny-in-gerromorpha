#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($hits_evalues,$transdecoder_folder,$blast_folder,$submission_folder,$phylogeny_names,$submission_queue_model,$queue_name,$queue_parallel_environment,$queue_nslots,$queue_command,$usegroups);

# Read user defined variables
GetOptions (    "hits_evalues=f" => , \$hits_evalues,
				"transdecoder_folder=s" => \$transdecoder_folder,
				"blast_folder=s" => \$blast_folder,
				"phylogeny_names=s" => \$phylogeny_names,
				"submission_folder=s" => \$submission_folder,
                "submission_queue_model=s" => \$submission_queue_model,
                "queue_name=s" => \$queue_name,
                "queue_parallel_environment=s" => \$queue_parallel_environment,
                "queue_command=s" => \$queue_command,
                "queue_nslots=i" => \$queue_nslots,
				"usegroups=i" => \$usegroups)
or die ("Error in command line arguments for Concatenate Sequences\n");

# Create script variables
my ($sp_num,$x,$y,$z,$queue_file_name);
my (%names,%new_names,%short_names);

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

if (! -d "$blast_folder"){system ("mkdir $blast_folder");}


$queue_file_name="$submission_folder/concatenate_contaminants.sh";
system ("cp submission_queue_model.txt $queue_file_name");
system ("sed -i 's/JOBNAME/concat_cont/' $queue_file_name");
system ("sed -i 's/QUEUE/$queue_name/' $queue_file_name");
system ("sed -i 's/ENVIRONMENT/$queue_parallel_environment/' $queue_file_name");
system ("sed -i 's/NSLOTS/$queue_nslots/' $queue_file_name");


# Concatenate blastp cleaned from contaminants
print "Preparing to concatenate cleaned blastp\n";
if (-e "$blast_folder/$sp_num\_concatenated.blastp.seq_yes-outfmt_6-evalue_$hits_evalues.out"){system ("echo 'rm $blast_folder/$sp_num\_concatenated.blastp.seq_yes-outfmt_6-evalue_$hits_evalues.out' >> $queue_file_name");}
$z=0;
foreach $x (sort { lc($a) cmp lc($b)} keys %names){
	$y=$new_names{$x};

	if (! -e "$blast_folder/$y.filtered.contaminants.blastp.out"){print "ERROR: $blast_folder/$y.filtered.contaminants.blastp.out doesn't exist\n";exit(0);}
	
	$z++;
	system ("echo 'echo \"\tDoing $y ($z out of $sp_num) (Concatenating cleaned blastp files)\"' >> $queue_file_name"); 
	system ("echo 'cat $blast_folder/$y.filtered.contaminants.blastp.out >> $blast_folder/$sp_num\_concatenated.blastp.seq_yes-outfmt_6-evalue_$hits_evalues.out' >> $queue_file_name");
}
system ("$queue_command $queue_file_name");



$queue_file_name="$submission_folder/concatenate_sequences.sh";
system ("cp submission_queue_model.txt $queue_file_name");
system ("sed -i 's/JOBNAME/concat_seq/' $queue_file_name");
system ("sed -i 's/QUEUE/$queue_name/' $queue_file_name");
system ("sed -i 's/ENVIRONMENT/$queue_parallel_environment/' $queue_file_name");
system ("sed -i 's/NSLOTS/$queue_nslots/' $queue_file_name");

# Concatenate files. Those files can contain contamintants because we'll now remove the contaminants after the blast to save time later. Also it will be used by SiLiX only to retrieve the secuences. Also, don't remove anything because I want to be sure I'm not introducing any error (alrady happened) and the concatenated.n is only used to rebuild the nucleotide alignments from protein alignments. The pipeline will follow externally with individual blast, clean blastp results from contaminants and merge. Launch this merge file with SiLiX. Remove gryllinae from the clusters and continue.
print "Preparint to concatenating aminoacid and nucleotide files\n";
if (-e "$blast_folder/$sp_num\_concatenated.fa"){system ("echo 'rm $blast_folder/$sp_num\_concatenated.fa' >> $queue_file_name");}
if (-e "$blast_folder/$sp_num\_concatenated.n.fa"){system ("echo 'rm $blast_folder/$sp_num\_concatenated.n.fa' >> $queue_file_name");}

$z=0;
foreach $x (sort { lc($a) cmp lc($b)} keys %names){
	if (! -e "$transdecoder_folder/$x.longest_orfs.filtered.pep.cd_hit.fa"){print "ERROR: $transdecoder_folder/$x.longest_orfs.filtered.pep.cd_hit.fa doesn't exist\n";exit(0);}
	if (! -e "$transdecoder_folder/$x.longest_orfs.filtered.cds"){print "ERROR: $transdecoder_folder/$x.longest_orfs.filtered.cds doesn't exist\n";exit(0);}

	$z++;system ("echo 'echo \"\tDoing $new_names{$x} ($z out of $sp_num) (Concatenating sequence files)\"' >> $queue_file_name"); 

	system ("echo 'cat $transdecoder_folder/$x.longest_orfs.filtered.pep.cd_hit.fa >> $blast_folder/$sp_num\_concatenated.fa' >> $queue_file_name");
	system ("echo 'cat $transdecoder_folder/$x.longest_orfs.filtered.cds >> $blast_folder/$sp_num\_concatenated.n.fa' >> $queue_file_name");
}

system ("echo 'echo \"Formatting blastdb (protein)\"' >> $queue_file_name");	
system ("echo 'makeblastdb -in $blast_folder/$sp_num\_concatenated.fa -parse_seqids -dbtype prot' >> $queue_file_name");
system ("echo 'echo \"Formatting blastdb (nucleotide)\"' >> $queue_file_name");	# We'll need it much later to build nucleotide alignment from protein alignments
system ("echo 'makeblastdb -in $blast_folder/$sp_num\_concatenated.n.fa -parse_seqids -dbtype nucl' >> $queue_file_name");
system ("$queue_command $queue_file_name");


