#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($nuclprot,$temp_folder,$blast_folder,$phylogeny_names,$selected_nodes,$usegroups);

# Read user defined variables
GetOptions (  	"temp_folder=s" => \$temp_folder,
				"blast_folder=s" => \$blast_folder,
                "phylogeny_names=s" => \$phylogeny_names,
				"selected_nodes=s" => \$selected_nodes,
				"nuclprot=s" => \$nuclprot,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for BLAST\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$queue_file_name);
my (@a,@array);
my (%names,%new_names,%short_names,%cont,%seqids,%hash,%selectednodes,%sequences);

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Load selected nodes
($x,$y)=&Selected_nodes($selected_nodes);
%seqids=%$x;
%selectednodes=%$y;


# Retrieving sequences from selected nodes
print "Retrieving sequences from selected nodes\n";
if ($nuclprot eq "prot"){if (! -e "$blast_folder/$sp_num\_concatenated.fa"){print "$blast_folder/$sp_num\_concatenated.fa doesn't exists\n";exit(0);}}
if ($nuclprot eq "nucl"){if (! -e "$blast_folder/$sp_num\_concatenated.n.fa"){print "$blast_folder/$sp_num\_concatenated.n.fa doesn't exists\n";exit(0);}}

open (SAVE,">$selected_nodes.ids");
foreach (sort {$a <=> $b} keys %seqids){print SAVE "$_\n";}close (SAVE);
if ($nuclprot eq "prot"){system ("blastdbcmd -db $blast_folder/$sp_num\_concatenated.fa -entry_batch $selected_nodes.ids > $selected_nodes.fa");}
if ($nuclprot eq "nucl"){system ("blastdbcmd -db $blast_folder/$sp_num\_concatenated.n.fa -entry_batch $selected_nodes.ids > $selected_nodes.fa");}
open (OPEN,"<$selected_nodes.fa");
while (<OPEN>){
	chomp $_;
	if ($_=~ /^>/){
		@_=split(/\s+/,$_);
		$_[0]=~ s/>//;
		$y=$_[0];
	}
	else {
		$sequences{$y}=$sequences{$y}.$_;
	}
}
close (OPEN);
open (SAVE,">$selected_nodes.$nuclprot");
foreach (keys %sequences){print SAVE "$_ -> $sequences{$_}\n";}
close (SAVE);
system ("rm $selected_nodes.fa");
