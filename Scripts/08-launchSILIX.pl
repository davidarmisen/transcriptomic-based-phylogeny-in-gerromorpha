#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($hits_evalues,$silix_min_blast_identity,$silix_min_blast_overlap,$silix_partial_length,$silix_partial_overlap,$temp_folder,$blast_folder,$submission_folder,$phylogeny_names,$fnodes,$submission_queue_model,$queue_name,$queue_parallel_environment,$queue_nslots,$queue_command,$usegroups);

# Read user defined variables
GetOptions (	"hits_evalues=f" => \$hits_evalues,
				"silix_min_blast_identity=s" => \$silix_min_blast_identity,
				"silix_min_blast_overlap=s" => \$silix_min_blast_overlap,
				"silix_partial_length=s" => \$silix_partial_length,
				"silix_partial_overlap=s" => \$silix_partial_overlap,
				"temp_folder=s" => \$temp_folder,
				"blast_folder=s" => \$blast_folder,
				"submission_folder=s" => \$submission_folder,
                "phylogeny_names=s" => \$phylogeny_names,
                "fnodes=s" => \$fnodes,
                "submission_queue_model=s" => \$submission_queue_model,
                "queue_name=s" => \$queue_name,
                "queue_parallel_environment=s" => \$queue_parallel_environment,
                "queue_command=s" => \$queue_command,
                "queue_nslots=i" => \$queue_nslots,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for SiLiX\n");

# Create script variables
my ($sp_num,$x,$y,$z,$queue_file_name);
my (%names,%new_names,%short_names);


# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Launch SiLiX to create the clusters 
print "Launch SiLiX\n";
$queue_file_name="$submission_folder/SiLiX.sh";

if (! -e "$blast_folder/$sp_num\_concatenated.fa"){print "ERROR: $blast_folder/$sp_num\_concatenated.fa doesn't exist\n";exit(0);}
if (! -e "$blast_folder/$sp_num\_concatenated.blastp.seq_yes-outfmt_6-evalue_$hits_evalues.out"){print "ERROR: $blast_folder/$sp_num\_concatenated.blastp.seq_yes-outfmt_6-evalue_$hits_evalues.out doesn't exist\n";exit(0);}

system ("cp submission_queue_model.txt $queue_file_name");
system ("sed -i 's/JOBNAME/CDH_$new_names{$_}/' $queue_file_name");
system ("sed -i 's/QUEUE/$queue_name/' $queue_file_name");
system ("sed -i 's/ENVIRONMENT/$queue_parallel_environment/' $queue_file_name");
system ("sed -i 's/NSLOTS/$queue_nslots/' $queue_file_name");

#$x="silix -i $silix_min_blast_identity -r $silix_min_blast_overlap -l $silix_partial_length -m $silix_partial_overlap $blast_folder/$sp_num\_concatenated.fa $blast_folder/$sp_num\_concatenated.blastp.seq_yes-outfmt_6-evalue_$hits_evalues.out > $temp_folder/seq-segfilter-$hits_evalues-$sp_num\_species-i$silix_min_blast_identity-r$silix_min_blast_overlap-l$silix_partial_length-m$silix_partial_overlap-repeat.fnodes";
$x="silix -i $silix_min_blast_identity -r $silix_min_blast_overlap -l $silix_partial_length -m $silix_partial_overlap $blast_folder/$sp_num\_concatenated.fa $blast_folder/$sp_num\_concatenated.blastp.seq_yes-outfmt_6-evalue_$hits_evalues.out > $temp_folder/$fnodes";
system ("echo '$x' >> $queue_file_name");

system ("$queue_command $queue_file_name");


