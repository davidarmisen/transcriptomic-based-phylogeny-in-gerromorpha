#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($hits_evalues,$hits_max_target_seqs,$transdecoder_folder,$transdecoder_filtered_folder,$blast_folder,$blast_filtered_folder,$phylogeny_names,$identity,$coverage,$usegroups);

# Read user defined variables
GetOptions (  	"hits_evalues=f" => \$hits_evalues,
                "hits_max_target_seqs=f" => \$hits_max_target_seqs,
				"transdecoder_folder=s" => \$transdecoder_folder,
				"transdecoder_filtered_folder=s" => \$transdecoder_filtered_folder,
				"blast_folder=s" => \$blast_folder,
				"blast_filtered_folder=s" => \$blast_filtered_folder,
                "phylogeny_names=s" => \$phylogeny_names,
				"identity=i" => \$identity,
				"coverage=s" => \$coverage,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for BLAST\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$queue_file_name);
my (%names,%new_names,%short_names,%cont,%size,@a,@b);

if (! -d $blast_filtered_folder){system ("mkdir $blast_filtered_folder");}
if (! -d $transdecoder_filtered_folder){system ("mkdir $transdecoder_filtered_folder");}

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Use blastn contaminants results to filter blastp 
print "Filter out contaminants from BLAST results\n";

# Check first that all files exists to save time if one is missing at the end so we don't need to repeat everything
$z=0;
print "Checking all files exists first\n";
foreach $x (sort { lc($a) cmp lc($b)} keys %names){
	$y=$new_names{$x};

    $z++;print "\tCheking $y ($z out of $sp_num) (Check files used to filter out contaminants)\n";

	# Check transdecoder files
	if (! -e "$transdecoder_folder/$x.longest_orfs.cds"){print "$transdecoder_folder/$x.longest_orfs.cds doesn't exist\n";exit(0);}
	if (! -e "$transdecoder_folder/$x.longest_orfs.pep.cd_hit.fa"){print "$transdecoder_folder/$x.longest_orfs.pep.cd_hit.fa doesn't exist\n";exit(0);}

	# Check contaminants files
	if (! -e "$blast_folder/$y.contaminant.blastn.out"){print "$blast_folder/$y.contaminant.blastn.out doesn't exist\n";exit(0);}
	# Check spA vs spB files
	foreach $xx (sort { lc($a) cmp lc($b)} keys %names){
		$yy=$new_names{$xx};
		if ($y ne $yy){
			if (! -e "$blast_folder/$y.$yy.blastp.out"){print "$blast_folder/$y.$yy.blastp.out doesn't exist\n";exit(0);}
		}
	}
}


# Load all transcripts size
$z=0;
@b=();
print "Loading transcripts size\n";
foreach $x (sort { lc($a) cmp lc($b)} keys %names){
	$y=$new_names{$x};

	$z++;print "\tCheking $y ($z out of $sp_num) (Loading transcripts size)\n";

	# Check contaminants files
	if (! -e "$transdecoder_folder/$x.longest_orfs.cds"){print "$transdecoder_folder/$x.longest_orfs.cds doesn't exist\n";exit(0);}
	@a=`awk 'BEGIN{FS=" "}{print \$1}' $transdecoder_folder/$x.longest_orfs.cds | sed -e 's/\\(^>.*\$\\)/#\\1|/' | tr -d "\\r" | tr -d "\\n" | sed -e 's/ //g'  | sed -e 's/\$/#/' | tr "|" "\\t" | tr "#" "\\n"  | sed -e '/^\$/d' | sed -e 's/>//' | awk '{print \$1,"\t",length(\$2)}'`;
	#print "@a\n";
	push @b , @a;
}
#print "@b\n";
foreach (@b){chomp $_;@_=split(/\s+/,$_);$size{$_[0]}=$_[1];}


# Load contaminants
$z=0;
print "Loading contaminants\n";
foreach $x (sort { lc($a) cmp lc($b)} keys %names){
	$y=$new_names{$x};

    $z++;print "\tDoing $y ($z out of $sp_num) (Load contaminants)\n";

	# Load contaminants
	if (! -e "$blast_folder/$y.contaminant.blastn.out"){print "$blast_folder/$y.contaminant.blastn.out doesn't exist\n";exit(0);}
	open (OPEN,"<$blast_folder/$y.contaminant.blastn.out");
	while (<OPEN>){
	    chomp $_;
		@_=split(/\t/,$_);
		if ($_[2]>=$identity && ($_[3]/$size{$_[0]})>$coverage){$cont{$_[0]}=1;}
	}
	close (OPEN);
}
close (SAVE);


# Filter and concatenate per species now
$z=0;
print "Filtering contaminants\n";
foreach $x (sort { lc($a) cmp lc($b)} keys %names){
	$y=$new_names{$x};

    $z++;print "\tDoing $y ($z out of $sp_num) (Filter out contaminants from BLAST and Sequences and save)\n";

	# Filter out contamintants from sequences and write results in a file
	if (! -e "$transdecoder_folder/$x.longest_orfs.cds"){print "$transdecoder_filtered_folder/$x.longest_orfs.cds doesn't exist\n";exit(0);}
	if (! -e "$transdecoder_folder/$x.longest_orfs.pep.cd_hit.fa"){print "$transdecoder_filtered_folder/$x.longest_orfs.pep.cd_hit.fa doesn't exist\n";exit(0);}
	@a=();
	@a=`awk 'BEGIN{FS=" "}{print \$1}' $transdecoder_folder/$x.longest_orfs.cds | sed -e 's/\\(^>.*\$\\)/#\\1|/' | tr -d "\\r" | tr -d "\\n" | sed -e 's/ //g'  | sed -e 's/\$/#/' | tr "|" "\\t" | tr "#" "\\n"  | sed -e '/^\$/d' | sed -e 's/>//' | awk '{print \$1,"\t",\$2}'`;
	open (SAVE,">$transdecoder_filtered_folder/$x.longest_orfs.filtered.cds");
	foreach (@a){
		chomp $_;
		@_=split(/\s+/,$_);
		if (!$cont{$_[0]}){
			print SAVE ">$_[0]\n$_[1]\n";
		}
	}
	close (SAVE);
	@a=();
	@a=`awk 'BEGIN{FS=" "}{print \$1}' $transdecoder_folder/$x.longest_orfs.pep.cd_hit.fa | sed -e 's/\\(^>.*\$\\)/#\\1|/' | tr -d "\\r" | tr -d "\\n" | sed -e 's/ //g'  | sed -e 's/\$/#/' | tr "|" "\\t" | tr "#" "\\n"  | sed -e '/^\$/d' | sed -e 's/>//' | awk '{print \$1,"\t",\$2}'`;
	open (SAVE,">$transdecoder_filtered_folder/$x.longest_orfs.filtered.pep.cd_hit.fa");
	foreach (@a){
		chomp $_;
		@_=split(/\s+/,$_);
		if (!$cont{$_[0]}){
			print SAVE ">$_[0]\n$_[1]\n";
		}
	}
	close (SAVE);


	# Filter out contamintants from BLAST and write results in a file
	open (SAVE,">$blast_filtered_folder/$y\.filtered.contaminants.blastp.out");
	foreach $xx (sort { lc($a) cmp lc($b)} keys %names){
		$yy=$new_names{$xx};

		if ($y ne $yy){
        	#$z++;print "\tDoing $y - $yy ($z out of ",$sp_num*($sp_num-1),") (Filter out contaminants)\n";
			if (! -e "$blast_folder/$y.$yy.blastp.out"){print "$blast_folder/$y.$yy.blastp.out doesn't exist\n";exit(0);}
			open (SAVE2,">$blast_filtered_folder/$y.$yy\.filtered.contaminants.blastp.out");
			open (OPEN,"<$blast_folder/$y.$yy.blastp.out");
			while (<OPEN>){
			    chomp $_;
				@_=split(/\t/,$_);
				if (!$cont{$_[0]} && !$cont{$_[1]}){print SAVE "$_\n";print SAVE2 "$_\n";}
			}
			close (OPEN);
			close (SAVE2);
		}
	}
	close (SAVE);
}
close (SAVE)
