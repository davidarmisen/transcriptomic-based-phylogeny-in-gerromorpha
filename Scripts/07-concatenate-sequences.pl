#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($hits_evalues,$transdecoder_folder,$blast_folder,$phylogeny_names,$usegroups);

# Read user defined variables
GetOptions (    "hits_evalues=f" => , \$hits_evalues,
		"transdecoder_folder=s" => \$transdecoder_folder,
		"blast_folder=s" => \$blast_folder,
		"phylogeny_names=s" => \$phylogeny_names,
		"usegroups=i" => \$usegroups)
or die ("Error in command line arguments for Concatenate Sequences\n");

# Create script variables
my ($sp_num,$x,$y,$z);
my (%names,%new_names,%short_names);

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

if (! -d "$blast_folder"){system ("mkdir $blast_folder");}


# Concatenate blastp cleaned from contaminants
print "Concatenating cleaned blastp\n";
if (-e "$blast_folder/$sp_num\_concatenated.blastp.seq_yes-outfmt_6-evalue_$hits_evalues.out"){system ("rm $blast_folder/$sp_num\_concatenated.blastp.seq_yes-outfmt_6-evalue_$hits_evalues.out");}
$z=0;
foreach $x (sort { lc($a) cmp lc($b)} keys %names){
	$y=$new_names{$x};

	if (! -e "$blast_folder/$y.filtered.contaminants.blastp.out"){print "ERROR: $blast_folder/$y.filtered.contaminants.blastp.out doesn't exist\n";exit(0);}

	$z++;print "\tDoing $y ($z out of $sp_num) (Concatenating cleaned blastp files)\n"; 

	system ("cat $blast_folder/$y.filtered.contaminants.blastp.out >> $blast_folder/$sp_num\_concatenated.blastp.seq_yes-outfmt_6-evalue_$hits_evalues.out");
}
#exit;


# Concatenate files.
print "Concatenating aminoacid and nucleotide files\n";
if (-e "$blast_folder/$sp_num\_concatenated.fa"){system ("rm $blast_folder/$sp_num\_concatenated.fa");}
if (-e "$blast_folder/$sp_num\_concatenated.n.fa"){system ("rm $blast_folder/$sp_num\_concatenated.n.fa");}

$z=0;
foreach $x (sort { lc($a) cmp lc($b)} keys %names){
	if (! -e "$transdecoder_folder/$x.longest_orfs.filtered.pep.cd_hit.fa"){print "ERROR: $transdecoder_folder/$x.longest_orfs.filtered.pep.cd_hit.fa doesn't exist\n";exit(0);}
	if (! -e "$transdecoder_folder/$x.longest_orfs.filtered.cds"){print "ERROR: $transdecoder_folder/$x.longest_orfs.filtered.cds doesn't exist\n";exit(0);}

	$z++;print "\tDoing $new_names{$x} ($z out of $sp_num) (Concatenating sequence files)\n"; 

	system ("cat $transdecoder_folder/$x.longest_orfs.filtered.pep.cd_hit.fa >> $blast_folder/$sp_num\_concatenated.fa");
	system ("cat $transdecoder_folder/$x.longest_orfs.filtered.cds >> $blast_folder/$sp_num\_concatenated.n.fa");
}

print "Formatting blastdb (protein)\n";	
system ("makeblastdb -in $blast_folder/$sp_num\_concatenated.fa -parse_seqids -dbtype prot");
print "Formatting blastdb (nucleotide)\n";	# We'll need it much later to build nucleotide alignment from protein alignments
system ("makeblastdb -in $blast_folder/$sp_num\_concatenated.n.fa -parse_seqids -dbtype nucl");




