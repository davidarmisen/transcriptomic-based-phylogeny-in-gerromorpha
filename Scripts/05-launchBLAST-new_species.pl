#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($new_species,$hits_evalues,$hits_max_target_seqs,$transdecoder_folder,$blast_folder,$submission_folder,$phylogeny_names,$submission_queue_model,$queue_name,$queue_parallel_environment,$queue_nslots,$queue_command,$usegroups);

# Read user defined variables
GetOptions (	"new_species=s" => \$new_species,
				"hits_evalues=f" => \$hits_evalues,
                "hits_max_target_seqs=f" => \$hits_max_target_seqs,
				"transdecoder_folder=s" => \$transdecoder_folder,
				"blast_folder=s" => \$blast_folder,
				"submission_folder=s" => \$submission_folder,
                "phylogeny_names=s" => \$phylogeny_names,
                "submission_queue_model=s" => \$submission_queue_model,
                "queue_name=s" => \$queue_name,
                "queue_parallel_environment=s" => \$queue_parallel_environment,
                "queue_command=s" => \$queue_command,
                "queue_nslots=i" => \$queue_nslots,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for BLAST\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$zz,$queue_file_name);
my (%names,%new_names,%short_names);
my ($new_sp_num,%new_sp_names,%new_sp_new_names,%new_sp_short_names);
my (%names1,%new_names1,%short_names1);
my (%names2,%new_names2,%short_names2);
my ($loop);

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Load new species names
($new_sp_num,$x,$y,$z)=&Names($new_species,$usegroups);
%new_sp_names=%$x;
%new_sp_new_names=%$y;
%new_sp_short_names=%$z;


# Launch independent BLASTs all against all (except themselves) (new species only)
print "Launch independent BLASTs all against all (except themselves) (new species only)\n";

for ($loop=0;$loop<=1;$loop++){
	if ($loop==0){
		%names1=%new_sp_names;
		%new_names1=%new_sp_new_names;
		%names2=%names;
		%new_names2=%new_names;
	}
	else {
		%names2=%new_sp_names;
		%new_names2=%new_sp_new_names;
		%names1=%names;
		%new_names1=%new_names;
	}

	$z=0;
	foreach $x (sort { lc($a) cmp lc($b)} keys %names1){
		$y=$new_names1{$x};
		foreach $xx (sort { lc($a) cmp lc($b)} keys %names2){
			$yy=$new_names2{$xx};

			if ($y ne $yy){
				$queue_file_name="$submission_folder/blast_$y\_$yy\.sh";

				if (! -e "$transdecoder_folder/$x.longest_orfs.pep.cd_hit.fa"){print "ERROR: $transdecoder_folder/$x.longest_orfs.pep.cd_hit.fa doesn't exist\n";exit(0);}
				if (! -e "$transdecoder_folder/$xx.longest_orfs.pep.cd_hit.fa"){print "ERROR: $transdecoder_folder/$xx.longest_orfs.pep.cd_hit.fa doesn't exist\n";exit(0);}

				$z++;print "\tDoing $y - $yy ($z out of ",$new_sp_num*($sp_num-1),") (BLAST all-vs-all in cluster)\n";

				system ("cp submission_queue_model.txt $queue_file_name");
				system ("sed -i 's/JOBNAME/BP_$y\_$yy/' $queue_file_name");
				system ("sed -i 's/QUEUE/$queue_name/' $queue_file_name");
				system ("sed -i 's/ENVIRONMENT/$queue_parallel_environment/' $queue_file_name");
				system ("sed -i 's/NSLOTS/$queue_nslots/' $queue_file_name");

				$zz="blastp -db $transdecoder_folder/$xx.longest_orfs.pep.cd_hit.fa -query $transdecoder_folder/$x.longest_orfs.pep.cd_hit.fa -out $blast_folder/$y.$yy.blastp.out -num_threads $queue_nslots -seg yes -evalue $hits_evalues -max_target_seqs $hits_max_target_seqs -outfmt 6";

				system ("echo '$zz' >> $queue_file_name");
				system ("$queue_command $queue_file_name");
	
			}
		}
	}
}
