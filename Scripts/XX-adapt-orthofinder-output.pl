#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($ortho_file,$fnodes,$fnodes_clean);

# Read user defined variables
GetOptions (    "ortho_file=s" => , \$ortho_file,
				"fnodes=s" => , \$fnodes,
				"fnodes_clean=s" => \$fnodes_clean)
or die ("Error in command line arguments for Concatenate Sequences\n");


my (@a,@b,$x,$y);
my ($hog,$og,$tree,$parent,$clade,%nodes);

open (OPEN,"<$ortho_file");
open (SAVE,">$fnodes");
open (CLEAN,">$fnodes_clean");
while (<OPEN>){
	chomp $_;
	#print "$_\n";
	if ($_=~ /^N0/){
		
		@a=split(/\t/,$_);
		$hog=shift @a;
		$og=shift @a;
		$tree=shift @a;
#		$parent=shift @a;	# This is sometime missing. Is safer to keep and filter later
#		$clade=shift @a;	# This is sometime missing. Is safer to	keep and filter later

		$x=join("\t",@a);
		$x=~ s/,/\t/g;
		$x=~ s/\s/\t/g;
		$x=~ s/\t+/\t/g;
	
		@b=split(/\t/,$x);

		$og=~ s/OG//;
		$hog=~ s/N0\.HOG//;
	
		print CLEAN "$hog ->";		
		foreach $y (@b){
			if ($y=~ /@/){
				print SAVE "$hog\t$y\n";
				print CLEAN " $y";
			}
		}
		print CLEAN "\n";
	}
}
close (OPEN);
close (SAVE);
close (CLEAN);
