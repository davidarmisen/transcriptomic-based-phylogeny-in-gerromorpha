#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";


# Create user defined variables
my ($orthofinder_sequences_folder,$transdecoder_folder,$blast_folder,$phylogeny_names,$usegroups);

# Read user defined variables
GetOptions (    "orthofinder_sequences_folder=s" => , \$orthofinder_sequences_folder,
				"transdecoder_folder=s" => \$transdecoder_folder,
				"blast_folder=s" => \$blast_folder,
				"phylogeny_names=s" => \$phylogeny_names,
				"usegroups=i" => \$usegroups)
or die ("Error in command line arguments for Concatenate Sequences\n");

# Create script variables
my ($sp_num,$x,$y,$z);
my (%names,%new_names,%short_names);

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

if (! -d "$blast_folder"){system ("mkdir $blast_folder");}

# Concatenate files.
print "Concatenating aminoacid and nucleotide files\n";
if (-e "$blast_folder/$sp_num\_concatenated.fa"){system ("rm $blast_folder/$sp_num\_concatenated.fa");}
if (-e "$blast_folder/$sp_num\_concatenated.n.fa"){system ("rm $blast_folder/$sp_num\_concatenated.n.fa");}

$z=0;
foreach $x (sort { lc($a) cmp lc($b)} keys %names){
	if (! -e "$orthofinder_sequences_folder/$new_names{$x}.completely.filtered.faa"){print "ERROR: $orthofinder_sequences_folder/$new_names{$x}.completely.filtered.faa doesn't exist\n";exit(0);}
	if (! -e "$orthofinder_sequences_folder/$new_names{$x}.completely.filtered.fna"){print "ERROR: $orthofinder_sequences_folder/$new_names{$x}.completely.filtered.fna doesn't exist\n";exit(0);}

	$z++;print "\tDoing $new_names{$x} ($z out of $sp_num) (Concatenating sequence files)\n"; 

	system ("cat $orthofinder_sequences_folder/$new_names{$x}.completely.filtered.faa >> $blast_folder/$sp_num\_concatenated.fa");
	system ("cat $orthofinder_sequences_folder/$new_names{$x}.completely.filtered.fna >> $blast_folder/$sp_num\_concatenated.n.fa");
}

print "Formatting blastdb (protein)\n";	
system ("makeblastdb -in $blast_folder/$sp_num\_concatenated.fa -parse_seqids -dbtype prot");
print "Formatting blastdb (nucleotide)\n";	# We'll need it much later to build nucleotide alignment from protein alignments
system ("makeblastdb -in $blast_folder/$sp_num\_concatenated.n.fa -parse_seqids -dbtype nucl");




