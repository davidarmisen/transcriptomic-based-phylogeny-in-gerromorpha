#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($nuclprot,$selected_nodes,$selected_sequences,$temp_folder,$blast_folder,$submission_folder,$phylogeny_names,$usegroups);

# Read user defined variables
GetOptions (  	"temp_folder=s" => \$temp_folder,
				"blast_folder=s" => \$blast_folder,
				"submission_folder=s" => \$submission_folder,
                "phylogeny_names=s" => \$phylogeny_names,
                "selected_nodes=s" => \$selected_nodes,
                "selected_sequences=s" => \$selected_sequences,
                "nuclprot=s" => \$nuclprot,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for cluster BLAST\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$zz,$queue_file_name);
my (@array);
my (%names,%new_names,%short_names,%seqids,%selectednodes,%sequences);


# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Load selected nodes
($x,$y)=&Selected_nodes($selected_nodes);
%seqids=%$x;
%selectednodes=%$y;

# Load selected sequences
($x)=&Selected_sequences($selected_sequences);
%sequences=%$x;

# Launch BLASTs for selected nodes
print "Launch BLASTs for selected nodes\n";
$z=0;
if (! -d "$blast_folder"){system ("mkdir $blast_folder");}
foreach $x (sort {$a <=> $b} keys %selectednodes){

	$z++;print "\tDoing cluster $x ($z out of ", scalar keys %selectednodes,") (BLAST selected cluster)\n";

	open (SAVE,">$blast_folder/$x.seq");
	@array=split(/\s+/,$selectednodes{$x});
	foreach (@array){print SAVE ">$_\n$sequences{$_}\n";}
	close (SAVE);

	$y="makeblastdb -dbtype $nuclprot -in $blast_folder/$x.seq -parse_seqids";
	system ("$y");
	$y="blastp -query $blast_folder/$x.seq -db $blast_folder/$x.seq -outfmt 6 -out $blast_folder/$x.blast";
	system ("$y");
}



