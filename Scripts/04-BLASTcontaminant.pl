#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($contaminant_evalue,$contaminant_max_target_seqs,$nt_file,$gis_file,$transdecoder_folder,$blast_folder,$phylogeny_names,$usegroups);

# Read user defined variables
GetOptions (  	"contaminant_evalue=f" => \$contaminant_evalue,
		"contaminant_max_target_seqs=f" => \$contaminant_max_target_seqs,
		"nt_file=s" => \$nt_file,
		"gis_file=s" => \$gis_file,
		"transdecoder_folder=s" => \$transdecoder_folder,
		"blast_folder=s" => \$blast_folder,
                "phylogeny_names=s" => \$phylogeny_names,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for BLAST contaminants\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$queue_file_name);
my (%names,%new_names,%short_names);

if (! -d $blast_folder){system ("mkdir $blast_folder");}

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Launch blast against gis of possible contaminants
print "Launch blast against possible contaminants\n";
$z=0;
foreach $x (sort { lc($a) cmp lc($b)} keys %short_names){

	$z++;print "\tDoing $x ($z out of $sp_num) (BLAST contaminants)\n";

	if ($gis_file ne "none"){
                system ("blastn -db $nt_file -gilist $gis_file -query $transdecoder_folder/$short_names{$x}.longest_orfs.cds -out $blast_folder/$y.$out_file.blastn.out -dust yes -evalue $contaminant_evalue -max_target_seqs $contaminant_max_target_seqs -outfmt 6");
	}
        else {
                system ("blastn -db $nt_file -query $transdecoder_folder/$short_names{$x}.longest_orfs.cds -out $blast_folder/$y.$out_file.blastn.out -dust yes -evalue $contaminant_evalue -max_target_seqs $contaminant_max_target_seqs -outfmt 6");
        }

}
