#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($hits_evalue,$temp_folder,$blast_folder,$phylogeny_names,$identity,$length,$fnodes,$fnodes_clean,$usegroups);

# Read user defined variables
GetOptions (  	"hits_evalue=f" => \$hits_evalue,
				"temp_folder=s" => \$temp_folder,
				"blast_folder=s" => \$blast_folder,
                "phylogeny_names=s" => \$phylogeny_names,
				"identity=i" => \$identity,
				"length=i" => \$length,
				"fnodes=s" => \$fnodes,
				"fnodes_clean=s" => \$fnodes_clean,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for CLEANSILIX\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$queue_file_name);
my (@a);
my (%names,%new_names,%short_names,%cont,%nodes);

if (! -d $blast_folder){system ("mkdir $blast_folder");}

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Use blastn contaminants results to filter SILIX clusters
print "Filter out other contaminants from SILIX clusters with at least $identity% identity\n";
$z=0;
%cont=();
open (SAVE,">$blast_folder/$sp_num\_concatenated.blastn.other_contaminant.dust_yes-outfmt_6-evalue_$hits_evalue-$identity\identical.out");
foreach $x (sort { lc($a) cmp lc($b)} keys %names){
	$y=$new_names{$x};

    $z++;print "\tDoing $y ($z out of $sp_num) (Filter out other contaminants)\n";

	# Load contaminants
	if (! -e "$blast_folder/$y.other_contaminant.blastn.out"){print "$blast_folder/$y.other_contaminant.blastn.out doesn't exist\n";exit(0);}
	open (OPEN,"<$blast_folder/$y.other_contaminant.blastn.out");
	while (<OPEN>){
	        chomp $_;
		@_=split(/\t/,$_);
		if ($_[2]>=$identity && $_[3]>$length){ 
			$cont{$_[0]}=1;
			print SAVE "$_\n";
		} 
	}
	close (OPEN);
}
close (SAVE);
		
# Loading SiLiX nodes and remove contaminants
print "Loading SiLiX nodes and remove contaminants\n";
if (! -e "$fnodes"){print "$fnodes doesn't exist\n";exit(0);}
open (OPEN,"<$fnodes");
while (<OPEN>){
	chomp $_;
	@_=split(/\s+/,$_);
	if (!$cont{$_[1]}){
		if ($nodes{$_[0]} eq undef){$nodes{$_[0]}=$_[1];}
		else {$nodes{$_[0]}=$nodes{$_[0]}." $_[1]";}
	}
}
close (OPEN);
open (SAVE,">$fnodes_clean");foreach (keys %nodes){print SAVE "$_ -> $nodes{$_}\n";}close SAVE;


