#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($nucl_mafft_folder,$mafft_folder,$nuclprot,$selected_nodes,$selected_sequences,$temp_folder,$blast_folder,$submission_folder,$phylogeny_names,$usegroups);

# Read user defined variables
GetOptions (  	"temp_folder=s" => \$temp_folder,
				"blast_folder=s" => \$blast_folder,
				"submission_folder=s" => \$submission_folder,
                "phylogeny_names=s" => \$phylogeny_names,
				"nucl_mafft_folder=s" => \$nucl_mafft_folder,
				"mafft_folder=s" => \$mafft_folder,
                "selected_nodes=s" => \$selected_nodes,
                "selected_sequences=s" => \$selected_sequences,
                "nuclprot=s" => \$nuclprot,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for cluster alignment\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$zz,$queue_file_name,$v,$w);
my (@array,@a,@b);
my (%names,%new_names,%short_names,%seqids,%selectednodes,%sequences,%select,%xa,%xb,%xc,%xd,%xe);

my (@c,@e,@d,@f,$r,$s,$t);

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Load selected nodes
($x,$y)=&Selected_nodes($selected_nodes);
%seqids=%$x;
%selectednodes=%$y;

# Load selected sequences
($x)=&Selected_sequences($selected_sequences);
%sequences=%$x;


# Launch GBLOCKS to concatenate later
print "Launch GBLOCKS\n";

#system ("module load Gblocks/0.91b");

if (! -d "$mafft_folder"){system ("mkdir $mafft_folder");}
if (-e "$mafft_folder/$sp_num\_gblocks.out"){system ("rm $mafft_folder/$sp_num\_gblocks.out");}

$y=scalar keys %selectednodes;  # Retrieve the number of keys in the hash
$z=0;

foreach $x (sort {$a <=> $b} keys %selectednodes){
	$z++;
	print "\tDoing $z out of $y (GBLOCKS)\n";
	@array=split(/\s+/,$selectednodes{$x});
	system("Gblocks $nucl_mafft_folder/$x.bestscores.sequences.mafft -t=c -b5=h;mv $nucl_mafft_folder/$x.bestscores.sequences.mafft-* $mafft_folder");
}

