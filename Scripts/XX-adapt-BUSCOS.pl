#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($busco_list,$busco_folder,$temp_folder,$transdecoder_folder,$transdecoder_filtered_folder,$blast_folder,$blast_filtered_folder,$phylogeny_names,$fnodes,$usegroups);
my ($contaminant_evalue,$contaminant_max_target_seqs,$nt_file_pre,$gis_file_pre,$out_file_pre,$nt_file_post,$gis_file_post,$out_file_post,$batch_command);

# Read user defined variables
GetOptions (  	"contaminant_evalue=f" => \$contaminant_evalue,
                "contaminant_max_target_seqs=f" => \$contaminant_max_target_seqs,
                "nt_file_pre=s" => \$nt_file_pre,
                "gis_file_pre=s" => \$gis_file_pre,
                "out_file_pre=s" => \$out_file_pre,
				"nt_file_post=s" => \$nt_file_post,
                "gis_file_post=s" => \$gis_file_post,
                "out_file_post=s" => \$out_file_post,
				"busco_list=s" => \$busco_list,
				"busco_folder=s" => \$busco_folder,
				"temp_folder=s" => \$temp_folder,
				"blast_folder=s" => \$blast_folder,
				"blast_filtered_folder=s" => \$blast_filtered_folder,
				"transdecoder_folder=s" => \$transdecoder_folder,
				"transdecoder_filtered_folder=s" => \$transdecoder_filtered_folder,
                "phylogeny_names=s" => \$phylogeny_names,
				"fnodes=s" => \$fnodes,
				"batch_command=s" => \$batch_command,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for adapt-BUSCOS\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$queue_file_name);
my (@a);
my (%names,%new_names,%short_names,%cont,%nodes);


# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

my ($xx,$yy,$zz,@aa,@bb,@cc,%hh1,%hh2,%index_names);


# Create fake transdecoder files
if (1==2){
	# Remove previous sequence files
	foreach $x (sort { lc($a) cmp lc($b)} keys %names){
		#print "$x\n";exit;
		if (-e "$transdecoder_folder/$x.longest_orfs.cds"){system("rm $transdecoder_folder/$x.longest_orfs.cds");}
		if (-e "$transdecoder_folder/$x.longest_orfs.pep"){system("rm $transdecoder_folder/$x.longest_orfs.pep");}
	}

	# Prepare files per species
	open (OPEN,"<$busco_list");
	open (SAVE,">$fnodes");
	while ($xx=<OPEN>){
		print "Doing $xx";
		chomp $xx;
		open (FILES,"<$busco_folder/$xx.fna");
		$x=0;
		while ($yy=<FILES>){
			chomp $yy;
			if ($yy=~ /^>/){
				$yy=~ s/>//g;
				@aa=split(/@/,$yy);
				print SAVE "$xx\t$yy\n";
			}
			elsif ($yy=~ /^[a-zA-Z]/){
				open (SEQ,">>$transdecoder_folder/$short_names{$aa[0]}.longest_orfs.cds");
				print SEQ ">$aa[0]\@$aa[1]\n$yy\n";
				close (SEQ);
			}
		}
		open (FILES,"<$busco_folder/$xx.faa");
		$x=0;
		while ($yy=<FILES>){
			chomp $yy;
			if ($yy=~ /^>/){
				$yy=~ s/>//g;
				@aa=split(/@/,$yy);
			}
			elsif ($yy=~ /^[a-zA-Z]/){
				open (SEQ,">>$transdecoder_folder/$short_names{$aa[0]}.longest_orfs.pep");
				print SEQ ">$aa[0]\@$aa[1]\n$yy\n";
				close (SEQ);
			}
		}
	}
	close (OPEN);
	close (SAVE);
}


# Launch blast contaminants on fake transdecoder sequences
if (1==1){
	# BLAST contaminant 
	$x="-contaminant_evalue=$contaminant_evalue -contaminant_max_target_seqs=$contaminant_max_target_seqs -out_file=$out_file_pre -nt_file=$nt_file_pre -gis_file=$gis_file_pre -transdecoder_folder=$transdecoder_folder -blast_folder=$blast_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#print "$x\n";exit;
	#system ("./04-BLASTcontaminant.pl $x");
	system ("./04-launchBLASTcontaminant.pl $x $batch_command");
}

# Launch blast other contaminants on fake transdecoder sequences
if (1==1){
	# BLAST other contaminant 
	$x="-contaminant_evalue=$contaminant_evalue -contaminant_max_target_seqs=$contaminant_max_target_seqs -out_file=$out_file_post -nt_file=$nt_file_post -gis_file=$gis_file_post -transdecoder_folder=$transdecoder_folder -blast_folder=$blast_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#print "$x\n";exit;
	#system ("./04-BLASTcontaminant.pl $x");
	system ("./04-launchBLASTcontaminant.pl $x $batch_command");
}

