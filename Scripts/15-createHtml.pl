#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($mafft_folder,$html_folder,$nuclprot,$selected_nodes,$selected_sequences,$temp_folder,$blast_folder,$submission_folder,$phylogeny_names,$submission_queue_model,$queue_name,$queue_parallel_environment,$queue_nslots,$queue_command,$usegroups);

# Read user defined variables
GetOptions (  	"temp_folder=s" => \$temp_folder,
				"blast_folder=s" => \$blast_folder,
				"submission_folder=s" => \$submission_folder,
                "phylogeny_names=s" => \$phylogeny_names,
				"mafft_folder=s" => \$mafft_folder,
				"html_folder=s" => \$html_folder,
                "selected_nodes=s" => \$selected_nodes,
                "selected_sequences=s" => \$selected_sequences,
                "nuclprot=s" => \$nuclprot,
                "submission_queue_model=s" => \$submission_queue_model,
                "queue_name=s" => \$queue_name,
                "queue_parallel_environment=s" => \$queue_parallel_environment,
                "queue_command=s" => \$queue_command,
                "queue_nslots=i" => \$queue_nslots,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for cluster alignment\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$zz,$queue_file_name);
my (@array);
my (%names,%new_names,%short_names,%seqids,%selectednodes,%sequences);

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Load selected nodes
($x,$y)=&Selected_nodes($selected_nodes);
%seqids=%$x;
%selectednodes=%$y;

# Load selected sequences
($x)=&Selected_sequences($selected_sequences);
%sequences=%$x;

# Create HTML files to check the alignments 
$z=0;
if (! -d "$html_folder"){system ("mkdir $html_folder");}
print " Create HTML files to check the alignments\n";
foreach $x (sort {$a <=> $b} keys %selectednodes){push @array,$x;}
for ($z=0;$z<=$#array;$z++){
	print "Doing hmtl for ",$z+1," out of ",$#array+1,"\n";
	$x="$mafft_folder/$array[$z].bestscores.sequences.mafft";
	$y="$html_folder/$array[$z].html";
	&FastaToHTML($x,$y,$array[$z-1],$array[$z+1]);
}

