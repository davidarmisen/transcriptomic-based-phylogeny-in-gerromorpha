#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($transdecoder_folder,$phylogeny_names,$usegroups);

# Read user defined variables
GetOptions (	"transdecoder_folder=s" => \$transdecoder_folder,
                "phylogeny_names=s" => \$phylogeny_names,
		"usegroups=i" => \$usegroups)
or die ("Error in command line arguments for CDHit\n");

# Create script variables
my ($sp_num,$x,$y,$z);
my (%names,%new_names,%short_names);

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Launch cd-hit to reduce the number of redundant transcripts due to Trinity's high number of nearly identical isotigs and isoforms 
print "Launch cd-hit to reduce redundancy\n";
$z=0;
foreach (sort { lc($a) cmp lc($b) } keys %names){
	if (! -e "$transdecoder_folder/$_.longest_orfs.pep"){print "ERROR: $transdecoder_folder/$_.longest_orfs.pep doesn't exist\n";exit(0);}
	$z++;print "\tDoing $_ ($z out of $sp_num) (CDHit)\n"; 
	system ("cd-hit -i $transdecoder_folder/$_.longest_orfs.pep -o $transdecoder_folder/$_.longest_orfs.pep.cd_hit.fa -c 0.995 -n 5 -T 1");	# -T Number of cores, -n Word size
	print "\tFormatting blastdb (protein)\n";      # We'll need it much later to build protein alignments
	system ("makeblastdb -in $transdecoder_folder/$_.longest_orfs.pep.cd_hit.fa -parse_seqids -dbtype prot");
}
$z=();
