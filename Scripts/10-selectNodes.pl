#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($max_size,$temp_folder,$blast_folder,$phylogeny_names,$species_in_cluster,$fnodes,$selected_nodes,$usegroups);

# Read user defined variables
GetOptions (  	"temp_folder=s" => \$temp_folder,
				"blast_folder=s" => \$blast_folder,
                "phylogeny_names=s" => \$phylogeny_names,
				"species_in_cluster=s" => \$species_in_cluster,
				"fnodes=s" => \$fnodes,
				"selected_nodes=s" => \$selected_nodes,
				"max_size=i" => \$max_size,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for selecSILIXnodes\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$queue_file_name);
my (@a,@array);
my (%names,%new_names,%short_names,%cont,%nodes,%seqids,%hash,%selectednodes);


# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Load nodes
($x)=&Load_nodes($fnodes);
%nodes=%$x;

# Looking for families with a representative of $species_in_cluster*100 % each species
print "Looking for families with a representative in ",$species_in_cluster*100,"% of the species\n";
$z=0;
foreach $x (sort {$a <=> $b} keys %nodes){
		%hash=();
		@array=split (/\s+/,$nodes{$x});
		if ($#array>=$sp_num*$species_in_cluster){	# Look for clusters where we potentially have enough species (regardless of isoforms)
			foreach (@array){
				$y=&Species($_);
				if ($short_names{$y} ne undef){
					$hash{$y}=1;
				}
			}
			$_=scalar keys %hash;
			if ($_>=($sp_num*$species_in_cluster)){	# The total number of different species
				if ($#array<=$max_size){ # Let's try to avoid too big files
					$selectednodes{$x}=$nodes{$x};
					foreach (@array){$seqids{$_}=$seqids{$_}+1;}
				}
			}
		}
}
open (SAVE,">$selected_nodes");foreach (keys %selectednodes){print SAVE "$_ -> $selectednodes{$_}\n";}close SAVE;

