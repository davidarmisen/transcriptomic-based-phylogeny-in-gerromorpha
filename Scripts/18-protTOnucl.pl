#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($nucl_mafft_folder,$mafft_folder,$nuclprot,$selected_nodes,$selected_sequences,$temp_folder,$blast_folder,$submission_folder,$phylogeny_names,$usegroups);

# Read user defined variables
GetOptions (  	"temp_folder=s" => \$temp_folder,
				"blast_folder=s" => \$blast_folder,
				"submission_folder=s" => \$submission_folder,
                "phylogeny_names=s" => \$phylogeny_names,
				"nucl_mafft_folder=s" => \$nucl_mafft_folder,
				"mafft_folder=s" => \$mafft_folder,
                "selected_nodes=s" => \$selected_nodes,
                "selected_sequences=s" => \$selected_sequences,
                "nuclprot=s" => \$nuclprot,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for cluster alignment\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$zz,$queue_file_name,$v,$w);
my (@array,@a,@b);
my (%names,%new_names,%short_names,%seqids,%selectednodes,%sequences,%select,%xa,%xb,%xc,%xd,%xe,%aacode);

my (@c,@e,@d,@f,$r,$s,$t);

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Load selected nodes
($x,$y)=&Selected_nodes($selected_nodes);
%seqids=%$x;
%selectednodes=%$y;

# Load selected sequences
($x)=&Selected_sequences($selected_sequences);
%sequences=%$x;

# Load aminoacid code
($x)=&AAcode();
%aacode=%$x;


# Create NUCLEOTIDES alignments from AMINOACID alignment
print "Create NUCLEOTIDES alignments from AMINOACID alignment\n";

if (! -d "$nucl_mafft_folder"){system ("mkdir $nucl_mafft_folder");}

$y=scalar keys %selectednodes;	# Retrieve the number of keys in the hash
@a=();
foreach $x (sort {$a <=> $b} keys %selectednodes){push @a,$x;}
for ($z=0;$z<=$#a;$z++){
	print "\tDoing PROT to NUCL alignment ",$z+1," out of ",$#a+1," (cluster $a[$z])\n";
	my $id;
	my @spe;
	my @ids;
	my $longest_seq=0;
	%_=();
	
	# Load protein sequences
	open (OPEN,"<$mafft_folder/$a[$z].bestscores.sequences.mafft");
	@b=();
	while (<OPEN>){
		chomp $_;
		if ($_=~ /^>/){
			@_=split(/\s+/,$_);
			$v=$_[0];
			@c=split (//,$_{$_[0]});
			$id=shift @_;
			push @ids, @_;
		}
		else {
			$_{$v}=$_{$v}.$_;
		}
	}
	close (OPEN);
	foreach (keys %_){if (length($_{$_})>$longest_seq){$longest_seq=length($_{$_});}}	

	# Retrieve nucleotide sequences
	$id=join(',',@ids);
	@b=`blastdbcmd -db $blast_folder/$sp_num\_concatenated.n.fa -entry $id`;

	%xb=();
	foreach (@b){
		chomp $_;
		if ($_=~ /^>/){
			@_=split(/\s+/,$_);
			$_[0]=~ s/>//g;
			$v=$_[0];
		}
		else {
			$xb{$v}=$xb{$v}.$_;
		}
	}

	# Retrieve nucleotide sequences		
	open (OPEN,"<$mafft_folder/$a[$z].bestscores.sequences.mafft");
	@b=();
	%xa=();
	while (<OPEN>){
		chomp $_;
		if ($_=~ /^>/){
			@_=split(/\s+/,$_);
			@c=split (//,$_{$_[0]});
			$id=shift @_;
			$r=0;
			@e=();@f=();
            $xa{$id}=$xb{$_[0]};
		}
	}
	close (OPEN);


	open (OPEN,"<$mafft_folder/$a[$z].bestscores.sequences.mafft");
	open (SAVE,">$nucl_mafft_folder/$a[$z].bestscores.sequences.mafft");
	@b=();
	while (<OPEN>){
		chomp $_;
		if ($_=~ /^>/){
			@_=split(/\s+/,$_);
			$v=$_[0];
			@c=split(//,$_{$v});
			@d=split(//,$xa{$v});
			$t=0; 
			print SAVE "$v\n";
			for ($s=0;$s<=$#c;$s++){
				if ($c[$s] eq '-'){print SAVE "---";}
				else {print SAVE "",$d[$t],"",$d[$t+1],"",$d[$t+2],"";$t=$t+3;}
			}
			print SAVE "\n";
		}
	}
	close (OPEN);
	close (SAVE);
}

