#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($trinity_folder,$submission_folder,$transdecoder_folder,$phylogeny_names,$submission_queue_model,$queue_name,$queue_parallel_environment,$queue_nslots,$queue_command,$usegroups);

# Read user defined variables
GetOptions (    "trinity_folder=s" => \$trinity_folder,
                "submission_folder=s" => \$submission_folder,
                "transdecoder_folder=s" => \$transdecoder_folder,
                "phylogeny_names=s" => \$phylogeny_names,
                "submission_queue_model=s" => \$submission_queue_model,
                "queue_name=s" => \$queue_name,
                "queue_parallel_environment=s" => \$queue_parallel_environment,
                "queue_command=s" => \$queue_command,
                "queue_nslots=i" => \$queue_nslots,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for Transdecoder\n");

# Create script variables
my ($sp_num,$x,$y,$z,$queue_file_name);
my (%names,%new_names,%short_names);


# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Launch TransDecoder
print "Launch TransDecoder using a cluster\n";
if (! -d "$transdecoder_folder"){system ("mkdir $transdecoder_folder");}
if (! -d "$submission_folder"){system ("mkdir $submission_folder");}

$z=0;
foreach (sort { lc($a) cmp lc($b)} keys %names){
	$queue_file_name="$submission_folder/transdecoder_$new_names{$_}\.sh";
	$z++;print "\tDoing $_ ($z out of $sp_num) (Transdecoder in cluster)\n"; 

	system ("cp submission_queue_model.txt $queue_file_name");
	system ("sed -i 's/JOBNAME/Trans_$new_names{$_}/' $queue_file_name");
	system ("sed -i 's/QUEUE/$queue_name/' $queue_file_name");
	system ("sed -i 's/ENVIRONMENT/$queue_parallel_environment/' $queue_file_name");
	system ("sed -i 's/NSLOTS/$queue_nslots/' $queue_file_name");

	$x="cp $trinity_folder/$_ $transdecoder_folder/;cd $transdecoder_folder;TransDecoder.LongOrfs -m 100 -t $_";

	system ("echo '$x' >> $queue_file_name");

	system ("$queue_command $queue_file_name");
}
