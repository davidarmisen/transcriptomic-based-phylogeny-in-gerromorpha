#!/usr/bin/perl
use strict;

# Load Aminoacid code
sub AAcode {
	my %aacode= (
		"TTT"=>"F","TCT"=>"S","TAT"=>"Y","TGT"=>"C",
		"TTC"=>"F","TCC"=>"S","TAC"=>"Y","TGC"=>"C",
		"TTA"=>"L","TCA"=>"S","TAA"=>"*","TGA"=>"*",
		"TTG"=>"L","TCG"=>"S","TAG"=>"*","TGG"=>"W",
		"CTT"=>"L","CCT"=>"P","CAT"=>"H","CGT"=>"R",
		"CTC"=>"L","CCC"=>"P","CAC"=>"H","CGC"=>"R",
		"CTA"=>"L","CCA"=>"P","CAA"=>"Q","CGA"=>"R",
		"CTG"=>"L","CCG"=>"P","CAG"=>"Q","CGG"=>"R",
		"ATT"=>"I","ACT"=>"T","AAT"=>"N","AGT"=>"S",
		"ATC"=>"I","ACC"=>"T","AAC"=>"N","AGC"=>"S",
		"ATA"=>"I","ACA"=>"T","AAA"=>"K","AGA"=>"R",
		"ATG"=>"M","ACG"=>"T","AAG"=>"K","AGG"=>"R",
		"GTT"=>"V","GCT"=>"A","GAT"=>"D","GGT"=>"G",
		"GTC"=>"V","GCC"=>"A","GAC"=>"D","GGC"=>"G",
		"GTA"=>"V","GCA"=>"A","GAA"=>"E","GGA"=>"G",
		"GTG"=>"V","GCG"=>"A","GAG"=>"E","GGG"=>"G"
	);
	return (\%aacode);
}

# Load names to use
sub Names {
	my $phylogeny_names=shift;	# Path to tabular file containing all names of species to use
	my $usegroups=shift // "0";		# Whether we want to use species names (0) or families (1). Default: 0

	if (!($usegroups==0 || $usegroups==1)){
		print "ERROR: Please select if you want to use species names (0) or families (1) at \$usegroup variable.\nYou selected $usegroups\n";
	}

	my (%names,%new_names,%short_names,%group1,%group1names,$sp_num);
	open (OPEN,"<$phylogeny_names") or die "Can't open $phylogeny_names\n";
	while (<OPEN>){
	        chomp $_;
	        @_=split (/\t/,$_);
	        if ($_!~ /^\//){
	                $names{$_[0]}=$_[1];
	                $new_names{$_[0]}=$_[2];
	                if ($_[3]){
	                        $group1{$_[1]}=$_[3];
	                        if (!$group1names{$_[3]}){$group1names{$_[3]}=$_[1];}
	                        else{$group1names{$_[3]}=$group1names{$_[3]}." $_[1]";}
	                }
	                else{$group1{$_[1]}=$_[1];$group1names{$_[1]}=$_[1];}
	        }
	}
	close (OPEN);

	# Pick the right names depending if we are using families or not
	if ($usegroups==1){%names=%group1names}
	
	# Check that we have no problems with shortcut names
	foreach (sort { lc($a) cmp lc($b)} keys %new_names){
	        if ($short_names{$new_names{$_}}){print "ERROR: identical shortcut names. Try to change your species names.\n$_ AND $short_names{$new_names{$_}} -> $new_names{$_}\n\n" ; exit(0); }
	        else {$short_names{$new_names{$_}}=$_;}
	}
	
	# To change dynamically the $sp_num
	if ($usegroups==0){$sp_num=scalar keys %names;}
	else {$sp_num=scalar keys %group1names;}
	
	return ($sp_num,\%names,\%new_names,\%short_names);
}


sub Species {
	my $usegroups;
	my %group1;
	@_=split(/\@/,$_[0]);
	@_=split(/\_/,$_[0]);
	$_[0]=~ s/>//;
	if ($_[1]!~ /^c[0-9]/){
		$_[0]=$_[0]."_$_[1]";
		if ($_[2]!~ /^c[0-9]/){
			$_[0]=$_[0]."_$_[2]";
			if ($_[3]!~ /^c[0-9]/){
				$_[0]=$_[0]."_$_[3]";
				if ($_[4]!~ /^c[0-9]/){
					$_[0]=$_[0]."_$_[4]";
				}
			}
		}
	}
	
	# Those lines where added to deal with some custom problems in our datasets. Should cause no problem with other datasets but can be safely commented
	if ($_[0]=~ /Limnoporus_dissortis_454/){$_[0]="Limnoporus_dissortis";}
	if ($_[0]=~ /_OGSv1_1/){$_[0]="Oncopeltus_fasciatus";}
	if ($_[0]=~ /_VELI/){$_[0]="Halovelia_sp";}
	if ($_[0]=~ /_HEBR/){$_[0]="Hebrus_nipponicus";}
	
	$_[0]=~ s/_*$//;	# Added to remove unwanted results when formating an already formated species name
	
	if ($usegroups==1 && $group1{$_[0]}){$_[0]=$group1{$_[0]};}	# Added to deal with groups (so simple and elegant ;-) )

	return $_[0];
}


# Load silix nodes
sub Load_nodes {
	my $fnodes=shift;      # Path to silix clusters file
	my (@a,@b,@c,%nodes);
	print "Loading precomputed file with SiLiX clusters with removed contaminants\n";
	if (! -e "$fnodes"){print "$fnodes doesn't exists\n";}
	open (OPEN,"<$fnodes");
	while (<OPEN>){
		chomp $_;
		@_=split(/\s+->\s+/,$_);
		@a=split(/\s+/,$_[1]);
		$_[1]=();
		@c=();
		foreach (@a){
			@b=split (/\:\:/,$_);
			push @c,$_;
		}
		if (@c){$nodes{$_[0]}="@c";}
	}
	close (OPEN);
	return (\%nodes);
}

# Load selected nodes
sub Selected_nodes {
	my $selected_nodes=shift;      # Path to selected nodes file
    my (%seqids,%selectednodes);
	print "Loading precomputed file with selected SiLiX clusters\n";
	if (! -e "$selected_nodes"){print "$selected_nodes doesn't exists\n";}
        open (OPEN,"<$selected_nodes");
        while (<OPEN>){
                chomp $_;
                @_=split(/\s+->\s+/,$_);
                $selectednodes{$_[0]}=$_[1];
                @_=split(/\s+/,$_[1]);
                @seqids{@_} = (1) x @_; # Create a hash which keys come from the array
        }
        close (OPEN);
	return (\%seqids,\%selectednodes);
}

# Load sequences from selected nodes
sub Selected_sequences {
	my $sequences_file=shift;      # Path to selected sequences file
	my (%sequences);
	open (OPEN,"<$sequences_file");
	while (<OPEN>){
		chomp $_;
		@_=split(/\s+->\s+/,$_);
		$sequences{$_[0]}=$_[1];
	}
	close (OPEN);
	return (\%sequences);
}

sub FastaToHTML {
	my $in=shift;
	my $out=shift;
	my $prev=shift;
	my $next=shift;

	my ($id,@spe);
	%_=();
	open (OPEN,"<$in");
	while (<OPEN>){
		chomp $_;
		if ($_=~ /^>/){@_=split(/\s+/,$_);$id=$_[0];push @spe,$_[0];}
		else{$_{$id}=$_{$id}.$_;}
	}
	close (OPEN);
	open (SAVE,">$out");
	print SAVE "<!DOCTYPE html>
	<html>
		<body>
		<font face='Courier'>\n";
	if ($prev){print SAVE "\t\t<a href='",$prev,".html'>Previous</a>\n";}
	if ($next){print SAVE "\t\t<a href='",$next,".html'>Next</a>\n";}
	print SAVE"\t\t	<table>
			<tr>
			<td nowrap>";
			foreach (@spe){print SAVE "$_<br>";}
			print SAVE"	</td>
			<td nowrap>";
				foreach (@spe){print SAVE "$_{$_}<br>";}
				print SAVE"	</td>
			</tr>
		</table>
		</font>
		</body>
	</html>";
	close (SAVE);
}

sub Load_sequences {
    my $blast_folder=shift;
    my $select_ref=shift;
    my $x=shift;
    my @a;

	# Retrieve now all blast values from all selected sequences vs all selected sequences 
	open (OPEN,"<$blast_folder/$x.blast");
	while (<OPEN>){
		chomp $_;
		@_=split(/\s+/,$_);
		if (%$select_ref{$_[0]} && %$select_ref{$_[1]}){push @a,"$_[$#_-1]\t$_[0]\t$_[1]";}
	}
	close (OPEN);
    return (\@a);
}

sub Divide {
    my $a_ref=shift;
    my (@b,%species,%xa,%xb);
    
    # Sort array first
	@$a_ref=sort {$a <=> $b} @$a_ref;

	# Let's now pick the two most dissimilar sequences and start to build up our two groups based on that
	@b=split(/\t/,pop(@$a_ref));
	$species{Species($b[1])}="1 $b[1]";
	$species{Species($b[2])}="2 $b[2]";
	foreach (@$a_ref){
		chomp $_;
		@_=split(/\t/,$_);
		if ($_[1] eq $b[1] && $species{Species($_[2])}==0){$species{Species($_[2])}="1 $_[2]";}
		elsif ($_[1] eq $b[2] && $species{Species($_[2])}==0){$species{Species($_[2])}="2 $_[2]";}
	}

	@b=();%xa=();%xb=();
	foreach (keys %species){
		@_=split (/\s+/,$species{$_});	# 1 Colombia_Bayeca_c25708_g1::Colombia_Bayeca_c25708_g1_i3::g.7684::m.7684 -> Colombia_Bayeca
		if ($_[0]==1){$xa{$_}=$_[1];}
		elsif ($_[0]==2){$xb{$_}=$_[1];}
	}

	if (scalar(keys(%xb))>scalar(keys(%xa))){
		%_=%xa;
		%xa=%xb;
		%xb=%_;
	}
	
	print "Group 1 size: ",scalar(keys(%xa)),"\t Group 2 Size: ",scalar(keys(%xb)),"\n";
	return (\%xa,\%xb);
}

sub Select_new{
	my $blast_folder=shift;
	my $short_names=shift;
	my $x=shift;
        my $save_scores=shift;
	my $save_sequences=shift;
	my $final=shift;	# To define if it is the first run, an improvement or if the cluster is already good/complete and we only want to recover the scores to save them
	my $xa_ref=shift;	# List of genes against we want to look for hits
	my $sequences=shift;	# Genes sequences

	my (@c,%select,%species,%score,%hitscore,$y,$v,$w,%xc,%xd,%sequences);
	%sequences=%$sequences;

	%_=();

	if ($xa_ref ne $blast_folder){foreach (keys %$xa_ref){$xc{%$xa_ref{$_}}=$_;$xd{$_}=%$xa_ref{$_};}}	# List of genes that we keep and we want to look hit against

   	# Now we'll check again all the blast results and calculate new scores to recover the sequences from missing species (in group 2) that will better fit in group 1
	# We'll extract now the 'best' sequence from each missing species AND in case of equally good, the longest one ### ATTENTION !!! Not implemented as I don't easily see how without tons of loopings
	open (OPEN,"<$blast_folder/$x.blast");
	while (<OPEN>){
		chomp $_;
		@c=split(/\t/,$_);
		$v=Species($c[0]);
		$w=Species($c[1]);

		if ($v ne $w){ 	# Shouldn't have self species blast but just in case
			# The idea is that we are going to select the sequences that give us the smallest sum for %score or the biggest sum for %hitscore in group 1 (choice later)
			if ( 	($final==0 && !$species{"$c[0] $w"}) ||		# First condition is dependent if we are in our first run or improving it
				($final==1 && (!$xd{$v} && $xc{$c[1]}) && !$_{"$c[0] $c[1]"}) ||  # If we the cluster is not already complete and we want to improve it && the first gene species was not in the list but the second gene is in the it && it is the first hit (highest) between these two hits to avoid smaller partial hits 
				($final==2 && $xc{$c[0]} && $xc{$c[1]} && !$_{"$c[0] $c[1]"})){  # If the cluster is already defined and we just want to recover the scores && first species is in the list && second species is in the list && it is the first hit (highest) between these two hits to avoid smaller partial hits 

				$_{"$c[0] $c[1]"}=$_;
				$species{"$c[0] $w"}=$_;
				$score{$c[0]}=$score{$c[0]}+$c[$#c-1];
				$hitscore{$c[0]}=$hitscore{$c[0]}+$c[$#c];
			}
		}
	}
	close (OPEN);


	if ($save_scores){open (SAVE,">$save_scores");}
	foreach (sort {$hitscore{$b} <=> $hitscore{$a}} keys %hitscore){
		$y=Species($_);
		if (%$short_names{$y}){		# Because it is possible that we ran blast against more species that we want now
			if ($save_scores){print SAVE "$_ -> $hitscore{$_} -> $score{$_}\n";}
			if (!$select{$y}){
				$select{$y}=$_;
			}
			else {
				if ($hitscore{$_}==$hitscore{$select{$y}} && 
				    length($sequences{$_})>length($sequences{$select{$y}})){
					$select{$y}=$_;
				}
			}
		}
	}
	if ($save_scores){close (SAVE);}
	if ($save_sequences){
		open (SAVE,">$save_sequences");
		foreach (keys %select){
			print SAVE ">$_ $select{$_}\n",$sequences{$select{$_}},"\n";
		}
		close (SAVE);
	}
   
	return (\%select,\%hitscore,\%score);
}

1;
