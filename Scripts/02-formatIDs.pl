#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($transdecoder_folder,$phylogeny_names,$usegroups);

# Read user defined variables
GetOptions (	"transdecoder_folder=s" => \$transdecoder_folder,
		"phylogeny_namesr=s" => \$phylogeny_names,
		"usegroups=i" => \$usegroups)
or die ("Error in command line arguments for formatID\n");


# Create script variables
my ($sp_num,$x,$y,$z);
my (%names,%new_names,%short_names);


# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Rename IDs in TransDecoder files
print "Rename IDs in TransDecoder files\n";
$z=0;
foreach $y (sort { lc($a) cmp lc($b)} keys %names){
	$z++;print "\tDoing $y ($z out of $sp_num) (Rename IDs)\n"; 
	$x=0;
	%_=();
	open (OPEN,"<$transdecoder_folder/$y.transdecoder_dir/longest_orfs.cds") or die "Can't open $transdecoder_folder/$y.transdecoder_dir/longest_orfs.cds\n";
	open (SAVE1,">$transdecoder_folder/$y.longest_orfs.cds");
	open (SAVE2,">$transdecoder_folder/$y.names_index_cds.txt");
	while (<OPEN>){
		chomp $_;
		if ($_=~ /^>/){
			$_=~ s/>//;				
			$x++;
			print SAVE1 ">$new_names{$y}\@$x\n";
			print SAVE2 "$new_names{$y}\@$x\t$_\n";
			@_=split(/ /,$_);
			$_{$_[0]}="$new_names{$y}\@$x";
		}
		else {print SAVE1 "$_\n";}
	}
	close (OPEN);
	close (SAVE1);
	close (SAVE2);
	open (OPEN,"<$transdecoder_folder/$y.transdecoder_dir/longest_orfs.pep") or die "Can't open $transdecoder_folder/$y.transdecoder_dir/longest_orfs.pep\n";
	open (SAVE1,">$transdecoder_folder/$y.longest_orfs.pep");
	open (SAVE2,">$transdecoder_folder/$y.names_index_pep.txt");
	while (<OPEN>){
		chomp $_;
		if ($_=~ /^>/){
			$_=~ s/>//;
			@_=split(/ /,$_);
			print SAVE1 ">$_{$_[0]}\n";
			print SAVE2 "$_{$_[0]}\t$_\n";
		}
		else {print SAVE1 "$_\n";}
	}
	close (OPEN);
	close (SAVE1);
	close (SAVE2);
}
