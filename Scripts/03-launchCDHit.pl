#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($transdecoder_folder,$submission_folder,$phylogeny_names,$submission_queue_model,$queue_name,$queue_parallel_environment,$queue_nslots,$queue_command,$usegroups);

# Read user defined variables
GetOptions (    "transdecoder_folder=s" => \$transdecoder_folder,
				"submission_folder=s" => \$submission_folder,
                "phylogeny_names=s" => \$phylogeny_names,
                "submission_queue_model=s" => \$submission_queue_model,
                "queue_name=s" => \$queue_name,
                "queue_parallel_environment=s" => \$queue_parallel_environment,
                "queue_command=s" => \$queue_command,
                "queue_nslots=i" => \$queue_nslots,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for CDHit\n");

# Create script variables
my ($sp_num,$x,$y,$z,$queue_file_name);
my (%names,%new_names,%short_names);


# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Launch cd-hit to reduce the number of redundant transcripts
print "Launch cd-hit to reduce redundancy\n";
$z=0;
foreach (sort { lc($a) cmp lc($b)} keys %names){
	$queue_file_name="$submission_folder/cdhit_$new_names{$_}\.sh";
	if (! -e "$transdecoder_folder/$_.longest_orfs.pep"){print "ERROR: $transdecoder_folder/$_.longest_orfs.pep doesn't exist\n";exit(0);}
	$z++;print "\tDoing $_ ($z out of $sp_num) (CDHit in cluster)\n";

	system ("cp submission_queue_model.txt $queue_file_name");
	system ("sed -i 's/JOBNAME/CDH_$new_names{$_}/' $queue_file_name");
	system ("sed -i 's/QUEUE/$queue_name/' $queue_file_name");
	system ("sed -i 's/ENVIRONMENT/$queue_parallel_environment/' $queue_file_name");
	system ("sed -i 's/NSLOTS/$queue_nslots/' $queue_file_name");

	$x="cd-hit -i $transdecoder_folder/$_.longest_orfs.pep -o $transdecoder_folder/$_.longest_orfs.pep.cd_hit.fa -c 0.995 -n 5 -T $queue_nslots";  # -T Number of cores, -n Word size
	system ("echo '$x' >> $queue_file_name");

    $x="makeblastdb -in $transdecoder_folder/$_.longest_orfs.pep.cd_hit.fa -parse_seqids -dbtype prot";
	system ("echo '$x' >> $queue_file_name");

	system ("$queue_command $queue_file_name");
}
