#!/usr/bin/perl
use strict;

# ATTENTION: Those scripts are not meant to be used directly but be used as guide to check the different steps we performed to select the gene sequences on which we built the phylogeny. It will be necessary to adapt all file and folder names and uncomment the desired script to launch it. When two versions of the same script exits, launchXXX version is meant to be launch on a computing cluster uisng i.e. SLURM or SGE. An example file for submission jobs is provided but must be adapted accordingly (submission_queue_model.txt). Step 17 tried to fill the alignment gaps but has been removed to avoid extending chimeric errors. This pipeline was originally designed to run SiLiX (Miele et al. 2011) but was subsequently adapted to run on BUSCOS (Simao et al. 2015, Waterhouse et al. 2018) and Orthofinder (Emms et al. 2019) outputs too. If you want to try all three methods, beware of creating independent folders for each of them as files might be overwriten. 

my $sp_num=104; # Number of species. It is used to create the folders. It is recalculated later using species file ($phylogeny_names), if both numbers don't match there could be problems.

my $trinity_folder='/home/XXX/Trinity_Assemblies';
my $temp_folder='/home/XXX//temporal_folder';	# Temporal folder to contain all the intermediate files  
my $submission_folder=".";
my $transdecoder_folder="$temp_folder/transdecoder";
my $transdecoder_filtered_folder="$temp_folder/transdecoder_filtered";
my $blast_folder="$temp_folder/blast";
my $blast_filtered_folder="$temp_folder/blast_filtered";
my $cluster_blast_folder="$temp_folder/blast-clusters";
my $phylogeny_names='species.tsv';	# Tabulated file containing the names of the species to build the phylogeny on. Format (tab separated): Species_Trinity.fasta	Full_species_names	Short_species_names	Family(optional). Example: Cylindrostethus_palmaris_Trinity.fasta	Cylindrostethus_palmaris	Cyl_pal	Cylindrostethus 
my $new_species='new_species_run.tsv'; # File containing the names of new species to add to a former run if necessary. This should avoid to repeat all-vs-all blast and run only those missing species. To be sure everything runs smoothly, those species should be added to $phylogeny_names and the pipeline ran with the former species names commented until this point. When runnning the blast the former species names should then be uncommented
my $busco_list="busco_list.txt"; 	# Optional if BUSCOS used: List of buscos numbers. A previous script should have created a faa and fna file of each busco 
my $busco_folder="busco_clusters_104";	# Optional if BUSCOS used: Folder containing the faa and fna files for each busco number
my $ortho_file="N0.tsv"; # Optional if Orthofinder used: Orthofinder output file
my $orthofinder_sequences_folder="/home/XXX/completely_filtered_sequences/"; # Optional if Orthofinder used: Folder containing sequences filtered of contaminants (both Bacteria/Other/Unclassified/Virus and Gryllinae/Acheta).
#my $fnodes="$temp_folder/seq-segfilter-$hits_evalue-$sp_num\_species-i$silix_min_blast_identity-r$silix_min_blast_overlap-l$silix_partial_length-m$silix_partial_overlap-repeat.fnodes";
#my $fnodes_clean="$temp_folder/seq-segfilter-$hits_evalue-$sp_num\_species-i$silix_min_blast_identity-r$silix_min_blast_overlap-l$silix_partial_length-m$silix_partial_overlap-repeat.fnodes.clean";
my $fnodes="$temp_folder/nodes.fnodes";
my $fnodes_clean="$temp_folder/nodes.fnodes.clean";
my $nt_file_pre="/home/XXX/ncbi/nt";
my $gis_file_pre="/home/XXX/ncbi/archaea_bacteria_other_unclassified_viruses_nt.gi";
my $nt_file_post="/home/XXX/ncbi/gryllinae.nodup.fa";
my $gis_file_post="none";
my $submission_queue_model='submission_queue_model.txt';
my $queue_name="E5-*";
my $queue_parallel_environment="openmp4";
my $queue_nslots="4";
my $queue_command="qsub";
my $usegroups=0;
my $contaminant_evalue=1e-5;
my $contaminant_max_target_seqs=1000;
my $hits_evalue=1e-5;
my $hits_max_target_seqs=1000;
my $identity=90; # Contaminants filter : identity>=$identity
my $coverage=0.25;	 # Contaminants filter : coverage>$coverage
my $silix_min_blast_identity=0.70;	# -i
my $silix_min_blast_overlap=0.80;	# -r
my $silix_partial_length=100;	# -l
my $silix_partial_overlap=0.50;	# -m
my $identity_other_contaminant=90; # Other contaminants filter (Gryllinae): identity>=$identity. 0 Acts as no filter. Not recommended
my $length_other_contaminant=0; # Other contaminants filter (Gryllinae): length>$length. 0 = Acts as no filter 
my $species_in_cluster=0.80; # Min. % of species in a cluster to select it
my $max_size=5000; #  maximum size of the cluster (including isoforms). This is used to avoid to analyze too big clusters which will yield no trustful information
my $selected_nodes="$temp_folder/selectednodes_$sp_num\_$species_in_cluster.txt";
my $nuclprot="prot"; # Select if we want to use nucleic or proteic alignment
my $sequences="$selected_nodes.$nuclprot";
my $scores_folder="$temp_folder/scores-$usegroups-$sp_num";
my $bestscores_folder="$temp_folder/bestscores-$usegroups-$sp_num";
my $mafft_folder="$temp_folder/mafft-$usegroups-$sp_num/";
my $html_folder="$temp_folder/html-$usegroups-$sp_num/";
my $improved_scores_folder="$temp_folder/scores-improved-$usegroups-$sp_num";
my $improved_bestscores_folder="$temp_folder/bestscores-improved-$usegroups-$sp_num";
my $improved_mafft_folder="$temp_folder/mafft-improved-$usegroups-$sp_num/";
my $nucl_improved_mafft_folder="$temp_folder/mafft-improved-$usegroups-$sp_num-nucl";
my $improved_html_folder="$temp_folder/html-improved-$usegroups-$sp_num/";
my $gblocks_improved_mafft_folder="$temp_folder/mafft-improved-gblock-$usegroups-$sp_num-codon/";
my $concatenated_folder="$temp_folder/concatenated-improved-$usegroups-$sp_num/";
my $nucl_concatenated_folder="$temp_folder/concatenated-improved-$usegroups-$sp_num-nucl/";
my $gblock_coverage=0.5;
my $gblock_length=100;


my ($x,$y,$z);

# Create temp_folder if it doesn't exists
if (! -d $temp_folder){system ("mkdir $temp_folder");}

# Define submission queue options
$y="-submission_folder=$submission_folder -submission_queue_model=$submission_queue_model -queue_name=$queue_name -queue_parallel_environment=$queue_parallel_environment -queue_nslots=$queue_nslots -queue_command=$queue_command";

# Transdecoder
$x="-trinity_folder=$trinity_folder -transdecoder_folder=$transdecoder_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./01-Transdecoder.pl $x");
#system ("./01-launchTransdecoder.pl $x $y");

# FormatID 
$x="-transdecoder_folder=$transdecoder_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./02-formatIDs.pl $x");

# CDHit
$x="-transdecoder_folder=$transdecoder_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./03-CDHit.pl $x");
#system ("./03-launchCDHit.pl $x $y");




#SiLiX
#####################
# BLAST contaminant "Bacteria, Other, Unclassified and Virus
$x="-contaminant_evalue=$contaminant_evalue -contaminant_max_target_seqs=$contaminant_max_target_seqs -out_file='contaminant' -nt_file=$nt_file_pre -gis_file=$gis_file_pre -transdecoder_folder=$transdecoder_folder -blast_folder=$blast_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./04-BLASTcontaminant.pl $x");
#system ("./04-launchBLASTcontaminant.pl $x $y");

# BLAST all-vs-all
$x="-hits_evalue=$hits_evalue -hits_max_target_seqs=$hits_max_target_seqs -transdecoder_folder=$transdecoder_folder -blast_folder=$blast_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./05-BLAST.pl $x");
#system ("./05-launchBLAST.pl $x $y");
#system ("./05-launchBLAST-new_species.pl -new_species=$new_species $x $y");

# CLEAN BLAST results and transdecoder sequences
$x="-hits_evalue=$hits_evalue -hits_max_target_seqs=$hits_max_target_seqs -identity=$identity -coverage=$coverage -transdecoder_folder=$transdecoder_folder -transdecoder_filtered_folder=$transdecoder_filtered_folder -blast_folder=$blast_folder -blast_filtered_folder=$blast_filtered_folder  -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./06-CLEAN.pl $x");
#system ("./06-launchCLEAN.pl $x $y");

# Concatenate Transdecoder sequences (used by SiLiX to retrieve nucleotide sequences) and cleaned BLAST results 
$x="-hits_evalue=$hits_evalue -transdecoder_folder=$transdecoder_filtered_folder -blast_folder=$blast_filtered_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./07-concatenate-sequences.pl $x");
#system ("./07-launchConcatenate-sequences.pl $x $y");

# SILIX (be sure the Boost library is loaded i.e. module load GCC/7.2.0/OpenMPI/3.0.0/Boost/1.66.0)
$x="-hits_evalue=$hits_evalue -silix_min_blast_identity=$silix_min_blast_identity -silix_min_blast_overlap=$silix_min_blast_overlap -silix_partial_length=$silix_partial_length -silix_partial_overlap=$silix_partial_overlap -temp_folder=$temp_folder -blast_folder=$blast_filtered_folder -phylogeny_names=$phylogeny_names -fnodes=$fnodes -usegroups=$usegroups";
#system ("./08-SILIX.pl $x");
#system ("./08-launchSILIX.pl $x $y");

# BLAST contaminant Gryllinae (We found of this contamination late during analysis and we decided to remove it there instead of repeating the whole blast all vs all run)
$x="-contaminant_evalue=$contaminant_evalue -contaminant_max_target_seqs=$contaminant_max_target_seqs -out_file='other_contaminant' -nt_file=$nt_file_post -gis_file=$gis_file_post -transdecoder_folder=$transdecoder_filtered_folder -blast_folder=$blast_filtered_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./04-BLASTcontaminant.pl $x");
#system ("./04-launchBLASTcontaminant.pl $x $y");

# Clean SILIX from other contaminant
$x="-identity=$identity_other_contaminant -length=$length_other_contaminant -fnodes=$fnodes -fnodes_clean=$fnodes_clean -temp_folder=$temp_folder -blast_folder=$blast_filtered_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./09-cleanSILIX.pl $x");
# NEXT: 10-selectNodes.pl




#BUSCOS
#####################
# Adapt BUSCOS output and clean contaminants (We assume the concatenated files have already been created)
# SUBSTITUTES: ./04-BLASTcontaminant.pl ./06-CLEAN.pl 07-concatenate-sequences.pl ./04-BLASTcontaminant.pl (other) ./09-cleanSILIX.pl
$x="-busco_list=$busco_list -busco_folder=$busco_folder -blast_folder=$blast_folder -transdecoder_folder=$transdecoder_folder -blast_filtered_folder=$blast_filtered_folder -transdecoder_filtered_folder=$transdecoder_filtered_folder -temp_folder=$temp_folder -fnodes=$fnodes -phylogeny_names=$phylogeny_names -contaminant_evalue=$contaminant_evalue -contaminant_max_target_seqs=$contaminant_max_target_seqs -out_file_pre='contaminant' -nt_file_pre=$nt_file_pre -gis_file_pre=$gis_file_pre -out_file_post='other_contaminant' -nt_file_post=$nt_file_post -gis_file_post=$gis_file_post -batch_command='$y' -usegroups=$usegroups";
#system ("./XX-adapt-BUSCOS.pl $x");

$x="-hits_evalue=$hits_evalue -hits_max_target_seqs=$hits_max_target_seqs -identity=$identity -coverage=$coverage -transdecoder_folder=$transdecoder_folder -transdecoder_filtered_folder=$transdecoder_filtered_folder -blast_folder=$blast_folder -blast_filtered_folder=$blast_filtered_folder  -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./XX-adapt-BUSCOS-CLEAN.pl $x");

$x="-transdecoder_filtered_folder=$transdecoder_filtered_folder -identity=$identity_other_contaminant -length=$length_other_contaminant -fnodes=$fnodes -fnodes_clean=$fnodes_clean -temp_folder=$temp_folder -blast_folder=$blast_filtered_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./XX-adapt-BUSCOS-CLEAN-NODES.pl $x");

$x="-busco_list=$busco_list -busco_folder=$busco_folder -blast_folder=$blast_folder -transdecoder_folder=$transdecoder_folder -blast_filtered_folder=$blast_filtered_folder -transdecoder_filtered_folder=$transdecoder_filtered_folder -temp_folder=$temp_folder -fnodes=$fnodes -fnodes_clean=$fnodes_clean -phylogeny_names=$phylogeny_names -contaminant_evalue=$contaminant_evalue -contaminant_max_target_seqs=$contaminant_max_target_seqs -out_file_pre='contaminant' -nt_file_pre=$nt_file_pre -gis_file_pre=$gis_file_pre -out_file_post='other_contaminant' -nt_file_post=$nt_file_post -gis_file_post=$gis_file_post -batch_command='$y' -usegroups=$usegroups";
#system ("./XX-adapt-BUSCOS-CHANGE-IDS.pl $x");
# NEXT: 10-selectNodes.pl





#ORTHOFINDER
#####################
# To save computing time we used Pre-Computed blast option in OrthoFinder. To do so we recovered SILiX BLAST all-vs-all outputs wich were already filtered of Archaea, Bacteria/Other/Unclassified/Virus contaminants on which we removed Gryllinae/Acheta contaminants.
# Same results can be found cleaning Transdecoder sequences from both contaminants and using them as inputs for OrthoFinder. To do so we can run the following three steps:

# BLAST contaminant "Archaea, Bacteria, Other, Unclassified and Virus
$x="-contaminant_evalue=$contaminant_evalue -contaminant_max_target_seqs=$contaminant_max_target_seqs -out_file='contaminant' -nt_file=$nt_file_pre -gis_file=$gis_file_pre -transdecoder_folder=$transdecoder_folder -blast_folder=$blast_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./04-BLASTcontaminant.pl $x");
#system ("./04-launchBLASTcontaminant.pl $x $y");

# BLAST contaminant Gryllinae (We found of this contamination late during analysis and we decided to remove it there instead of repeating the whole blast all vs all run)
$x="-contaminant_evalue=$contaminant_evalue -contaminant_max_target_seqs=$contaminant_max_target_seqs -out_file='other_contaminant' -nt_file=$nt_file_post -gis_file=$gis_file_post -transdecoder_folder=$transdecoder_filtered_folder -blast_folder=$blast_filtered_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./04-BLASTcontaminant.pl $x");
#system ("./04-launchBLASTcontaminant.pl $x $y");

# Sequences can then filtered with XX-adapt-ORTHOFINDER-CLEAN.pl and used as input for Orthofinder.
$x="-hits_evalue=$hits_evalue -hits_max_target_seqs=$hits_max_target_seqs -identity=$identity -coverage=$coverage-length=$length_other_contaminant -transdecoder_folder=$transdecoder_folder -transdecoder_filtered_folder=$transdecoder_filtered_folder -blast_folder=$blast_folder -blast_filtered_folder=$blast_filtered_folder  -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./XX-adapt-ORTHOFINDER-CLEAN.pl $x");

# Launch Orthofinder. No specific script 

# Adapt ORTHOFINDER output
$x="-ortho_file=$ortho_file -fnodes=$fnodes -fnodes_clean=$fnodes_clean";
#system ("xx-adapt-orthofinder-output.pl $x");
$x="-orthofinder_sequences_folder=$orthofinder_sequences_folder -transdecoder_folder=$transdecoder_filtered_folder -blast_folder=$blast_filtered_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("07-concatenate-sequences-ORTHOFINDER.pl $x");
# NEXT: 10-selectNodes.pl





#####################
# COMMON
#####################

# Select nodes with at least XX % of species
$x="-selected_nodes=$selected_nodes -max_size=$max_size -species_in_cluster=$species_in_cluster -fnodes=$fnodes_clean -temp_folder=$temp_folder -blast_folder=$blast_filtered_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./10-selectNodes.pl $x");

# Retrieving sequences from selected nodes ## ATTENTION: This script needs a file containing all the concatenate sequences. Normally the file is created by script 07.
$x="-nuclprot=$nuclprot -selected_nodes=$selected_nodes -temp_folder=$temp_folder -blast_folder=$blast_filtered_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./11-retrievingsequences.pl $x");

# Launch cluster BLAST # Results from this BLAST will be used to select a single gene per species on next scripts.
$x="-nuclprot=$nuclprot -selected_nodes=$selected_nodes -selected_sequences=$sequences -temp_folder=$temp_folder -blast_folder=$cluster_blast_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./12-BLASTcluster.pl $x");
#system ("./12-launchBLASTcluster.pl $x $y");

# Retrieve sequences from selected nodes blast and do a first selection
$x="-scores_folder=$scores_folder -bestscores_folder=$bestscores_folder -nuclprot=$nuclprot -selected_nodes=$selected_nodes -selected_sequences=$sequences -temp_folder=$temp_folder -blast_folder=$cluster_blast_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./13-firstSelection.pl $x $y");

# Align first selection # This step is very slow if not lunched in a cluster 
$x="-recover_names=0 -mafft_folder=$mafft_folder -scores_folder=$scores_folder -bestscores_folder=$bestscores_folder -nuclprot=$nuclprot -selected_nodes=$selected_nodes -selected_sequences=$sequences -temp_folder=$temp_folder -blast_folder=$cluster_blast_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./14-alignSelection.pl $x");
#system ("./14-launchAlignSelection.pl $x $y");

# Create html files to easily check the alignment
$x="-mafft_folder=$mafft_folder -html_folder=$html_folder -nuclprot=$nuclprot -selected_nodes=$selected_nodes -selected_sequences=$sequences -temp_folder=$temp_folder -blast_folder=$cluster_blast_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./15-createHtml.pl $x $y");

# Try to improve each cluster. It might happen that SiLiX cluster contain two (or three) groups of paralogous genes. This should not be a problem with Orthofinder and BUSCOS.
$x="-improved_scores_folder=$improved_scores_folder -improved_bestscores_folder=$improved_bestscores_folder -scores_folder=$scores_folder -bestscores_folder=$bestscores_folder -nuclprot=$nuclprot -selected_nodes=$selected_nodes -selected_sequences=$sequences -temp_folder=$temp_folder -blast_folder=$cluster_blast_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./16-improveSelection.pl $x");
#system ("./16-launchImproveSelection.pl $x $y");

# Align improved selection AND recover names # This step is very slow if not lunched in a cluster 
$x="-recover_names=0 -mafft_folder=$improved_mafft_folder -scores_folder=$improved_scores_folder -bestscores_folder=$improved_bestscores_folder -nuclprot=$nuclprot -selected_nodes=$selected_nodes -selected_sequences=$sequences -temp_folder=$temp_folder -blast_folder=$cluster_blast_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./14-alignSelection.pl $x");
#system ("./14-launchAlignSelection.pl $x $y");

# Create html files to easily check the alignment
$x="-mafft_folder=$improved_mafft_folder -html_folder=$improved_html_folder -nuclprot=$nuclprot -selected_nodes=$selected_nodes -selected_sequences=$sequences -temp_folder=$temp_folder -blast_folder=$cluster_blast_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./15-createHtml.pl $x $y");

# Create NUCLEOTIDES alignments from AMINOACID alignment
$x="-nucl_mafft_folder=$nucl_improved_mafft_folder -mafft_folder=$improved_mafft_folder -selected_nodes=$selected_nodes -selected_sequences=$sequences -temp_folder=$temp_folder -blast_folder=$blast_filtered_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./18-protTOnucl.pl $x");

# Launch GBLOCKS # Be sure Glocks is either loaded on the path
$x="-nucl_mafft_folder=$nucl_improved_mafft_folder -mafft_folder=$gblocks_improved_mafft_folder -selected_nodes=$selected_nodes -selected_sequences=$sequences -temp_folder=$temp_folder -blast_folder=$cluster_blast_folder -phylogeny_names=$phylogeny_names -usegroups=$usegroups";
#system ("./19-gblocks.pl $x");

# Filter and preconcatenate GBLOCKS
$x="-species_in_cluster=$species_in_cluster -nucl_concatenated_folder=$nucl_concatenated_folder -concatenated_folder=$concatenated_folder -nucl_mafft_folder=$nucl_improved_mafft_folder -mafft_folder=$gblocks_improved_mafft_folder -selected_nodes=$selected_nodes -selected_sequences=$sequences -temp_folder=$temp_folder -blast_folder=$cluster_blast_folder -phylogeny_names=$phylogeny_names -gblock_coverage=$gblock_coverage -gblock_length=$gblock_length -usegroups=$usegroups";
#system ("./20-filter_and_concatenate.pl $x");

