#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($trinity_folder,$transdecoder_folder,$phylogeny_names,$usegroups);

# Read user defined variables
GetOptions (	"trinity_folder=s" => \$trinity_folder,
		"transdecoder_folder=s" => \$transdecoder_folder,
		"phylogeny_names=s" => \$phylogeny_names,
		"usegroups=i" => \$usegroups)
or die ("Error in command line arguments for Transdecoder\n");

# Create script variables
my ($sp_num,$x,$y,$z);
my (%names,%new_names,%short_names);

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Launch TransDecoder
print "Launch TransDecoder\n";
if (! -d "$transdecoder_folder"){system ("mkdir $transdecoder_folder");}
$z=0;
foreach (sort { lc($a) cmp lc($b)} keys %names){
	$z++;print "\tDoing $_ ($z out of $sp_num) (Transdecoder)\n"; 
	system ("cp $trinity_folder/$_ $transdecoder_folder/");
	system ("cd $transdecoder_folder/ ; TransDecoder.LongOrfs -m 100 -t $_");
}
