#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($scores_folder,$bestscores_folder,$nuclprot,$selected_nodes,$selected_sequences,$temp_folder,$blast_folder,$submission_folder,$phylogeny_names,$submission_queue_model,$queue_name,$queue_parallel_environment,$queue_nslots,$queue_command,$usegroups);

# Read user defined variables
GetOptions (  	"temp_folder=s" => \$temp_folder,
				"blast_folder=s" => \$blast_folder,
				"submission_folder=s" => \$submission_folder,
                "phylogeny_names=s" => \$phylogeny_names,
                "scores_folder=s" => \$scores_folder,
                "bestscores_folder=s" => \$bestscores_folder,
                "selected_nodes=s" => \$selected_nodes,
                "selected_sequences=s" => \$selected_sequences,
                "nuclprot=s" => \$nuclprot,
                "submission_queue_model=s" => \$submission_queue_model,
                "queue_name=s" => \$queue_name,
                "queue_parallel_environment=s" => \$queue_parallel_environment,
                "queue_command=s" => \$queue_command,
                "queue_nslots=i" => \$queue_nslots,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for cluster first Selection\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$zz,$queue_file_name);
my (@array);
my (%names,%new_names,%short_names,%seqids,%selectednodes,%sequences);

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Load selected nodes
($x,$y)=&Selected_nodes($selected_nodes);
%seqids=%$x;
%selectednodes=%$y;

# Load selected sequences
($x)=&Selected_sequences($selected_sequences);
%sequences=%$x;

if (! -d "$scores_folder"){system ("mkdir $scores_folder");}
if (! -d "$bestscores_folder"){system ("mkdir $bestscores_folder");}

# Retrieve sequences from selected nodes blast and do a first selection
print "Retrieve sequences from selected nodes blast and do a first selection\n"; # Removed the size filter
$z=0;
$y=scalar keys %selectednodes;	# Retrieve the number of keys in the hash
foreach $x (sort {$a <=> $b} keys %selectednodes){
	$z++;
	print "\tDoing node $x ($z out of $y) (First selection)\n";
	&Select_new($blast_folder,\%short_names,$x,"$scores_folder/$x.scores","$bestscores_folder/$x.bestscores.sequences.$nuclprot",0,$blast_folder,\%sequences); 
}


