#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($improved_scores_folder,$improved_bestscores_folder,$scores_folder,$bestscores_folder,$nuclprot,$selected_nodes,$selected_sequences,$temp_folder,$blast_folder,$submission_folder,$phylogeny_names,$submission_queue_model,$queue_name,$queue_parallel_environment,$queue_nslots,$queue_command,$usegroups);

# Read user defined variables
GetOptions (  	"temp_folder=s" => \$temp_folder,
				"blast_folder=s" => \$blast_folder,
				"submission_folder=s" => \$submission_folder,
                "phylogeny_names=s" => \$phylogeny_names,
				"improved_scores_folder=s" => \$improved_scores_folder,
                "improved_bestscores_folder=s" => \$improved_bestscores_folder,
				"scores_folder=s" => \$scores_folder,
                "bestscores_folder=s" => \$bestscores_folder,
                "selected_nodes=s" => \$selected_nodes,
                "selected_sequences=s" => \$selected_sequences,
                "nuclprot=s" => \$nuclprot,
                "submission_queue_model=s" => \$submission_queue_model,
                "queue_name=s" => \$queue_name,
                "queue_parallel_environment=s" => \$queue_parallel_environment,
                "queue_command=s" => \$queue_command,
                "queue_nslots=i" => \$queue_nslots,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for cluster alignment\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$zz,$queue_file_name,$v,$w);
my (@array,@a,@b);
my (%names,%new_names,%short_names,%seqids,%selectednodes,%sequences,%select,%xa,%xb,%xc,%xd,%xe);

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Load selected nodes
($x,$y)=&Selected_nodes($selected_nodes);
%seqids=%$x;
%selectednodes=%$y;

# Load selected sequences
($x)=&Selected_sequences($selected_sequences);
%sequences=%$x;

# Try to improve each cluster (it is quite common to have two (or three) groups of different genes on each cluster)
print "Try to improve each cluster (it is quite common to have two (or three) groups of different genes on each cluster)\n";

if (! -d "$improved_bestscores_folder"){system ("mkdir $improved_bestscores_folder");}
if (! -d "$improved_scores_folder"){system ("mkdir $improved_scores_folder");}

$z=0;
$y=scalar keys %selectednodes;	# Retrieve the number of keys in the hash
foreach $x (sort {$a <=> $b} keys %selectednodes){
	$z++;
	#print "\tTrying to improve node $x ($z out of $y)\n";

	open (SCRIPT,">$x\_improve_selection.pl");
	print SCRIPT "#!/usr/bin/perl
	use strict;
	use Getopt::Long;
	require \"./subroutines.txt\";	
	my (\$v,\$w,%xa,%xb,%xc,%xd,%xe,%select,\@a,\@b);
	%select=();
	
	# Load names
	my (\$sp_num,\$x,\$y,\$z,%names,%new_names,%short_names);
	(\$sp_num,\$x,\$y,\$z)=&Names(\"$phylogeny_names\",\"$usegroups\");
	%names=%\$x;
	%new_names=%\$y;
	%short_names=%\$z;

	# Load selected nodes
	my (%seqids,%selectednodes);
	(\$x,\$y)=&Selected_nodes(\"$selected_nodes\");
	%seqids=%\$x;
	%selectednodes=%\$y;


	# Load selected sequences
	my (%sequences);
	(\$x)=&Selected_sequences(\"$selected_sequences\");
	%sequences=%\$x;

	# Retrieve first which sequence did we selected foreach species
	open (OPEN,\"<$bestscores_folder/$x.bestscores.sequences.$nuclprot\");
	while (<OPEN>){	
		chomp \$_;
		if (\$_=~ /^>/){
			\$_=~ s/>//;
			\@_=split (/\\s+/,\$_);
			\$select{\$_[1]}=\$_[0];	# Save current sequences selected in the cluster
		}
	}
	close (OPEN);

	# Retrieve now all blast values from all selected sequences vs all selected sequences 
	(\$v)=&Load_sequences(\"$blast_folder\",\\%select,\"$x\");
	\@a=\@\$v;
		
	# Let's now pick the two most dissimilar sequences and start to build up our two groups based on that
	(\$v,\$w)=&Divide(\\\@a);
    %xa=%\$v;%xb=%\$w;

	# Do again the two previous steps to do a division by 3 as in some case it is the case and I don't think it would be a too big problem to discard and find again some good sequences when there are only two groups
	%_=();
	foreach (keys %xa){\$_{\$xa{\$_}}=\$_;}
	(\$v)=&Load_sequences(\"$blast_folder\",\\%_,\"$x\");
	\@b=\@\$v;
	(\$v,\$w)=&Divide(\\\@b);
	%xc=%\$v;%xd=%\$w;
		
	# Let's now try to fix the problem by looking for new good sequences for each species on small cluster
	(\$v)=&Select_new(\"$blast_folder\",\\%short_names,\"$x\",'','',1,\\%xc,\\%sequences);  # 3 Groups 
	%xa=%xc;
	%xe=%\$v;

	%xc=(%xa,%xe);	# Combine both
	
	# Let's also calculate again scores and save them for the new cluster
	&Select_new(\"$blast_folder\",\\%short_names,\"$x\",\"$improved_scores_folder/$x.scores\",\"$improved_bestscores_folder/$x.bestscores.sequences.$nuclprot\",2,\\%xc,\\%sequences); 
	";
	close (SCRIPT);
	
    $queue_file_name="$submission_folder/improve_selection_$x\.sh";
    system ("cp submission_queue_model.txt $queue_file_name");
    system ("sed -i 's/JOBNAME/IS_$x/' $queue_file_name");
    system ("sed -i 's/QUEUE/$queue_name/' $queue_file_name");
    system ("sed -i 's/ENVIRONMENT/$queue_parallel_environment/' $queue_file_name");
    system ("sed -i 's/NSLOTS/$queue_nslots/' $queue_file_name");

    system ("chmod +x $x\_improve_selection.pl");
	system ("echo './$x\_improve_selection.pl' >> $queue_file_name");
	#print "\tTrying to improve node $x ($z out of $y)\n";
    system ("$queue_command $queue_file_name");
}
