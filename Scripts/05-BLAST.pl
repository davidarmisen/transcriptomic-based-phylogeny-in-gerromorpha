#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($hits_evalues,$hits_max_target_seqs,$transdecoder_folder,$blast_folder,$phylogeny_names,$usegroups);

# Read user defined variables
GetOptions (	"hits_evalues" => \$hits_evalues,
                "hits_max_target_seqs" => \$hits_max_target_seqs,
				"transdecoder_folder=s" => \$transdecoder_folder,
				"blast_folder=s" => \$blast_folder,
                "phylogeny_names=s" => \$phylogeny_names,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for BLAST\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$queue_file_name);
my (%names,%new_names,%short_names);


# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Launch independent BLASTs all against all (except themselves)
print "Launch independent BLASTs all against all (except themselves)\n";
$z=0;
foreach $x (sort { lc($a) cmp lc($b)} keys %names){
	$y=$new_names{$x};

	foreach $xx (sort { lc($a) cmp lc($b)} keys %names){
		$yy=$new_names{$xx};


		if ($y ne $yy){
			$queue_file_name="blast_$y\_$yy\.sh";

			if (! -e "$transdecoder_folder/$x.longest_orfs.pep.cd_hit.fa"){print "ERROR: $transdecoder_folder/$x.longest_orfs.pep.cd_hit.fa doesn't exist\n";exit(0);}
			if (! -e "$transdecoder_folder/$xx.longest_orfs.pep.cd_hit.fa"){print "ERROR: $transdecoder_folder/$xx.longest_orfs.pep.cd_hit.fa doesn't exist\n";exit(0);}
            $z++;print "\tDoing $y - $yy ($z out of ",$sp_num*($sp_num-1),") (BLAST all-vs-all)\n";

			system ("blastp -db $transdecoder_folder/$xx.longest_orfs.pep.cd_hit.fa -query $transdecoder_folder/$x.longest_orfs.pep.cd_hit.fa -out $blast_folder/$y.$yy.blastp.out -seg yes -evalue $hits_evalues -max_target_seqs $hits_max_target_seqs -outfmt 6");

		}
	}
}
