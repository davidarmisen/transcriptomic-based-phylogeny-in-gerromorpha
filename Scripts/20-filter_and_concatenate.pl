#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($species_in_cluster,$concatenated_folder,$nucl_concatenated_folder,$nucl_mafft_folder,$mafft_folder,$nuclprot,$selected_nodes,$selected_sequences,$temp_folder,$blast_folder,$submission_folder,$phylogeny_names,$gblock_coverage,$gblock_length,$usegroups);

# Read user defined variables
GetOptions (  	"temp_folder=s" => \$temp_folder,
				"species_in_cluster=s" => \$species_in_cluster,
				"blast_folder=s" => \$blast_folder,
				"submission_folder=s" => \$submission_folder,
                "phylogeny_names=s" => \$phylogeny_names,
				"nucl_mafft_folder=s" => \$nucl_mafft_folder,
				"concatenated_folder=s" => \$concatenated_folder,
				"nucl_concatenated_folder=s" => \$nucl_concatenated_folder,
				"mafft_folder=s" => \$mafft_folder,
                "selected_nodes=s" => \$selected_nodes,
                "selected_sequences=s" => \$selected_sequences,
                "nuclprot=s" => \$nuclprot,
				"gblock_coverage=s" => \$gblock_coverage,
				"gblock_length=s" => \$gblock_length,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for cluster alignment\n");

# Need to implement this


# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$zz,$queue_file_name,$v,$w);
my (@array,@a,@b);
my (%names,%new_names,%short_names,%seqids,%selectednodes,%sequences,%select,%xa,%xb,%xc,%xd,%xe,%hash,%aacode);

my (@c,@e,@d,@f,$r,$s,$t);

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Load selected nodes
($x,$y)=&Selected_nodes($selected_nodes);
%seqids=%$x;
%selectednodes=%$y;

# Load selected sequences
($x)=&Selected_sequences($selected_sequences);
%sequences=%$x;

# Load aminoacid code
($x)=&AAcode();
%aacode=%$x;


# Filter GBLOCKS and create pre-concatenated files fillinig missing species
print "Filter GBLOCKS and create pre-concatenated files filling missing species\n";

if (! -d "$concatenated_folder"){system ("mkdir $concatenated_folder");}
if (! -d "$nucl_concatenated_folder"){system ("mkdir $nucl_concatenated_folder");}
	
%hash=();%xa=();%xb=();%xc=();
$y=scalar keys %selectednodes;	# Retrieve the number of keys in the hash
$z=0;
foreach $x (sort {$a <=> $b} keys %selectednodes){
	$z++;
	print "\tDoing $z out of $y (Filtering Gblocks)\n";
	@array=split(/\s+/,$selectednodes{$x});
	open (OPEN,"<$nucl_mafft_folder/$x.bestscores.sequences.mafft");
	
	$v=();
	@a=();
	$w=0;
	while (<OPEN>){
		chomp $_;
		if ($_=~ /^>/){
			if ($w==1){
				$xa{$x}=length($hash{$x.$v});
				last;
			}
			@_=split(/\s+/,$_);
			$v=$_[0];
			$w++;
		}
		else {
			$hash{$x.$v}=$hash{$x.$v}.$_;
		}
	}
	close (OPEN);

	# Retrieve GBLOCKS size to check if the GBLOCKS % is ok (>gblock_coverage)
	open (OPEN,"<$mafft_folder/$x.bestscores.sequences.mafft-gb");
	$v=();
	@a=();
	$w=0;
	%hash=();
	while (<OPEN>){
		chomp $_;
		if ($_=~ /^>/){
			if ($w==1){
				$xb{$x}=length($hash{$x.$v});
				last;
			}
			@_=split(/\s+/,$_);
			$v=$_[0];
		$w++;
		}
		else {
			$_=~ s/\s//g;
			$hash{$x.$v}=$hash{$x.$v}.$_;
		}
	}	
	close (OPEN);

foreach (keys %xa){if ($xb{$_}/$xa{$_}>=$gblock_coverage && $xb{$_}>=$gblock_length){$xc{$_}=$xb{$_}/$xa{$_}}}

open (SAVE,">$temp_folder/selectednodes_$sp_num\_$gblock_coverage\_$gblock_length\_gblocks.txt");foreach (keys %xc){print SAVE "$_\n";};close (SAVE);
	
my %selectednodes=%xc; # Change values of selectednodes because we have removed some cases not fulfilling GBLOCKS filter 
	
@a=();foreach (keys %short_names){push @a,$_;};@a=sort{$a cmp $b}@a; # Just to have always the same order
$y=scalar keys %selectednodes;$z=0;  # Retrieve the number of keys in the hash


print "Number of clusters GBLOCKS passing the >=gblock_coverage filter: $y\n";

# Creating the preliminary files with the correct order 
foreach $x (sort {$a <=> $b} keys %selectednodes){

	$z++;
	print "\tDoing $z out of $y - $x (Creating files)\n";
	if ($xc{$x}){ # Previously filtered to >=gblock_coverage
		$v=();
		%hash=();
		# Retrieve all the sequences in the file
		open (OPEN,"<$mafft_folder/$x.bestscores.sequences.mafft-gb");
		while (<OPEN>){
			chomp $_;
			if ($_=~ /^>/){
				@_=split(/\s+/,$_);
				$v=$_[0];
				$v=~ s/>//g;
			}
			else {
				$_=~ s/\s//g;
				$hash{$v}=$hash{$v}.$_;
			}
		}
		close (OPEN);

		# Retrieve gblock alignment length
		@_=keys %hash;
		my $longest_seq=length($hash{$_[0]});

		open (SAVEGENE1,">$nucl_concatenated_folder/$x.bestscore.concatenated.txt");
		open (SAVEGENE2,">$concatenated_folder/$x.bestscore.concatenated.txt");

		foreach $w (@a){
			if ($hash{$w}){
				print SAVEGENE1 ">$w\n$hash{$w}\n";
				@d=split(//,$hash{$w});
				$t=();
					for ($s=0;$s<=$#d;$s=$s+3){
					if ($d[$s] eq "-" && $d[$s+1] eq "-" && $d[$s+2] eq "-" ){$t=$t."-"}
					elsif ($d[$s] eq "N" || $d[$s+1] eq "N" || $d[$s+2] eq "N" ){$t=$t."-"}	# We have some N in some cases. We can try to guess the base if it is the 3rd nucl or just ignore it
					elsif ($d[$s] ne "-" && $d[$s+1] ne "-" && $d[$s+2] ne "-" ){
						$t=$t.$aacode{$d[$s].$d[$s+1].$d[$s+2]};
					}
					else {print "ATENTION: It seems there is an error in the mafft alignment\n";exit;}
                }
				print SAVEGENE2 ">$w\n$t\n";
			}
			else {
				print SAVEGENE1 ">$w\n";
				print SAVEGENE1 '-' x $longest_seq;
				print SAVEGENE1 "\n";

				print SAVEGENE2 ">$w\n";
				print SAVEGENE2 '-' x ($longest_seq/3);
				print SAVEGENE2 "\n";

			}
		}
		close (SAVEGENE1);
		close (SAVEGENE2);
	}
}

#exit;
# Check the cluster's sequences that fulfill the SRH tests : SRHtests.sh 



