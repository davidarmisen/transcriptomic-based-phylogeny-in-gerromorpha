#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($hits_evalues,$silix_min_blast_identity,$silix_min_blast_overlap,$silix_partial_length,$silix_partial_overlap,$temp_folder,$blast_folder,$submission_folder,$phylogeny_names,$fnodes,$usegroups);

# Read user defined variables
GetOptions (	"hits_evalues=f" => \$hits_evalues,
				"silix_min_blast_identity=s" => \$silix_min_blast_identity,
				"silix_min_blast_overlap=s" => \$silix_min_blast_overlap,
				"silix_partial_length=s" => \$silix_partial_length,
				"silix_partial_overlap=s" => \$silix_partial_overlap,
				"temp_folder=s" => \$temp_folder,
				"blast_folder=s" => \$blast_folder,
				"submission_folder=s" => \$submission_folder,
                "phylogeny_names=s" => \$phylogeny_names,
				"fnodes=s" => \$fnodes,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for SiLiX\n");

# Create script variables
my ($sp_num,$x,$y,$z,$queue_file_name);
my (%names,%new_names,%short_names);


# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Launch SiLiX to create the clusters 
print "Launch SiLiX\n";

if (! -e "$blast_folder/$sp_num\_concatenated.fa"){print "ERROR: $blast_folder/$sp_num\_concatenated.fa doesn't exist\n";exit(0);}
if (! -e "$blast_folder/$sp_num\_concatenated.blastp.seq_yes-outfmt_6-evalue_$hits_evalues.out"){print "ERROR: $blast_folder/$sp_num\_concatenated.blastp.seq_yes-outfmt_6-evalue_$hits_evalues.out doesn't exist\n";exit(0);}

#$x="silix -i $silix_min_blast_identity -r $silix_min_blast_overlap -l $silix_partial_length -m $silix_partial_overlap $blast_folder/$sp_num\_concatenated.fa $blast_folder/$sp_num\_concatenated.blastp.seq_yes-outfmt_6-evalue_$hits_evalues.out > $temp_folder/seq-segfilter-$hits_evalues-$sp_num\_species-i$silix_min_blast_identity-r$silix_min_blast_overlap-l$silix_partial_length-m$silix_partial_overlap-repeat.fnodes";
$x="silix -i $silix_min_blast_identity -r $silix_min_blast_overlap -l $silix_partial_length -m $silix_partial_overlap $blast_folder/$sp_num\_concatenated.fa $blast_folder/$sp_num\_concatenated.blastp.seq_yes-outfmt_6-evalue_$hits_evalues.out > $temp_folder/$fnodes"
system ($x);



