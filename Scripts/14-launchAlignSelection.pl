#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($recover_names,$mafft_folder,$scores_folder,$bestscores_folder,$nuclprot,$selected_nodes,$selected_sequences,$temp_folder,$blast_folder,$submission_folder,$phylogeny_names,$submission_queue_model,$queue_name,$queue_parallel_environment,$queue_nslots,$queue_command,$usegroups);

# Read user defined variables
GetOptions (  	"temp_folder=s" => \$temp_folder,
				"recover_names=i" => \$recover_names,
				"blast_folder=s" => \$blast_folder,
				"submission_folder=s" => \$submission_folder,
                "phylogeny_names=s" => \$phylogeny_names,
                "mafft_folder=s" => \$mafft_folder,
                "scores_folder=s" => \$scores_folder,
                "bestscores_folder=s" => \$bestscores_folder,
                "selected_nodes=s" => \$selected_nodes,
                "selected_sequences=s" => \$selected_sequences,
                "nuclprot=s" => \$nuclprot,
				"submission_queue_model=s" => \$submission_queue_model,
                "queue_name=s" => \$queue_name,
                "queue_parallel_environment=s" => \$queue_parallel_environment,
                "queue_command=s" => \$queue_command,
                "queue_nslots=i" => \$queue_nslots,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for cluster alignment\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$zz,$queue_file_name);
my (@array);
my (%names,%new_names,%short_names,%seqids,%selectednodes,%sequences);

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Load selected nodes
($x,$y)=&Selected_nodes($selected_nodes);
%seqids=%$x;
%selectednodes=%$y;

# Load selected sequences
($x)=&Selected_sequences($selected_sequences);
%sequences=%$x;

# Align each cluster
print "Align each cluster selection\n";
if (! -d "$mafft_folder"){system ("mkdir $mafft_folder");}
$z=0;
$y=scalar keys %selectednodes;  # Retrieve the number of keys in the hash
foreach $x (sort {$a <=> $b} keys %selectednodes){
	$z++;

	open (SCRIPT,">$x\_align_selection.pl");
	print SCRIPT "#!/usr/bin/perl\nuse strict;my (\%cont);\n";

    print SCRIPT "system (\"mafft --auto --reorder $bestscores_folder/$x.bestscores.sequences.$nuclprot > $mafft_folder/$x.bestscores.sequences.mafft\");";

	if ($recover_names==1){
		# mafft cuts off names too long which can be a problem in our filled gaps versions where the names of the concatenated isotigs are kept. Therefore we need a small script to recover the original full names
		print SCRIPT "
                %_=();
                open (OPEN,\"<$bestscores_folder/$x.bestscores.sequences.$nuclprot\");
                while (<OPEN>){
                        if (\$_=~ /^>/){
                                \@_=split (/\\s+/,\$_);
                                \$_{\$_[0]}=\$_;
                        }
                }
                close (OPEN);
                open (OPEN,\"<$bestscores_folder/$x.bestscores.sequences.mafft\");
                open (SAVE,\">$temp_folder/temp-$usegroups-$x\");
                while (<OPEN>){
                        if (\$_=~ /^>/){
                                \@_=split (/\s+/,\$_);
                                print SAVE \"\$_{\$_[0]}\";
                        }
                        else {print SAVE \$_;}
                }
                close (OPEN);
                close (SAVE);
                system (\"mv $temp_folder/temp-$usegroups-$x $bestscores_folder/$x.bestscores.sequences.mafft\");
		";
	}

	close (SCRIPT);

	$queue_file_name="$submission_folder/align_selection_$x\.sh";
    system ("cp submission_queue_model.txt $queue_file_name");
    system ("sed -i 's/JOBNAME/BP_$y\_$yy/' $queue_file_name");
    system ("sed -i 's/QUEUE/$queue_name/' $queue_file_name");
    system ("sed -i 's/ENVIRONMENT/$queue_parallel_environment/' $queue_file_name");
    system ("sed -i 's/NSLOTS/$queue_nslots/' $queue_file_name");

    system ("chmod +x $x\_align_selection.pl");
    system ("echo './$x\_align_selection.pl' >> $queue_file_name");
#   print "\tMAFFT of node $x ($z out of $y) (Launch align selection)\n";
    system ("$queue_command $queue_file_name");
}

