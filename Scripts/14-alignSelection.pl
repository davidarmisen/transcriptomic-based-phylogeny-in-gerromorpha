#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($recover_names,$mafft_folder,$scores_folder,$bestscores_folder,$nuclprot,$selected_nodes,$selected_sequences,$temp_folder,$blast_folder,$submission_folder,$phylogeny_names,$usegroups);

# Read user defined variables
GetOptions (  	"temp_folder=s" => \$temp_folder,
				"recover_names=i" => \$recover_names,
				"blast_folder=s" => \$blast_folder,
				"submission_folder=s" => \$submission_folder,
                "phylogeny_names=s" => \$phylogeny_names,
                "mafft_folder=s" => \$mafft_folder,
                "scores_folder=s" => \$scores_folder,
                "bestscores_folder=s" => \$bestscores_folder,
                "selected_nodes=s" => \$selected_nodes,
                "selected_sequences=s" => \$selected_sequences,
                "nuclprot=s" => \$nuclprot,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for cluster alignment\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$zz,$queue_file_name);
my (@array);
my (%names,%new_names,%short_names,%seqids,%selectednodes,%sequences);

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Load selected nodes
($x,$y)=&Selected_nodes($selected_nodes);
%seqids=%$x;
%selectednodes=%$y;

# Load selected sequences
($x)=&Selected_sequences($selected_sequences);
%sequences=%$x;

# Align each cluster
print "Align each cluster selection\n";
if (! -d "$mafft_folder"){system ("mkdir $mafft_folder");}
$z=0;
$y=scalar keys %selectednodes;  # Retrieve the number of keys in the hash
foreach $x (sort {$a <=> $b} keys %selectednodes){
	$z++;
	print "\tMAFFT of node $x ($z out of $y) (Align selection)\n";
    system ("mafft --auto --reorder $bestscores_folder/$x.bestscores.sequences.$nuclprot > $mafft_folder/$x.bestscores.sequences.mafft");
	if ($recover_names==1){
		# mafft cuts off names too long which can be a problem in our filled gaps versions where the names of the concatenated isotigs are kept. Therefore we need a small script to recover the original full names
                %_=();
                open (OPEN,"<$bestscores_folder/$x.bestscores.sequences.$nuclprot");
                while (<OPEN>){
                        if ($_=~ /^>/){
                                @_=split (/\s+/,$_);
                                $_{$_[0]}=$_;
                        }
                }
                close (OPEN);
                #foreach (keys %_){print "$_ -> $_{$_}";}exit;
                open (OPEN,"<$bestscores_folder/$x.bestscores.sequences.mafft");
                open (SAVE,">$temp_folder/temp-$usegroups");
                while (<OPEN>){
                        if ($_=~ /^>/){
                                @_=split (/\s+/,$_);
                                print SAVE "$_{$_[0]}";
                        }
                        else {print SAVE $_;}
                }
                close (OPEN);
                close (SAVE);
                system ("mv $temp_folder/temp-$usegroups $bestscores_folder/$x.bestscores.sequences.mafft");
	}
}

