#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($contaminant_evalue,$contaminant_max_target_seqs,$out_file,$nt_file,$gis_file,$transdecoder_folder,$blast_folder,$submission_folder,$phylogeny_names,$submission_queue_model,$queue_name,$queue_parallel_environment,$queue_nslots,$queue_command,$usegroups);

# Read user defined variables
GetOptions (	"contaminant_evalue=f" => \$contaminant_evalue,
                "contaminant_max_target_seqs=f" => \$contaminant_max_target_seqs,
	  	"out_file=s" => \$out_file,
		"nt_file=s" => \$nt_file,
		"gis_file=s" => \$gis_file,
		"transdecoder_folder=s" => \$transdecoder_folder,
		"blast_folder=s" => \$blast_folder,
		"submission_folder=s" => \$submission_folder,
                "phylogeny_names=s" => \$phylogeny_names,
                "submission_queue_model=s" => \$submission_queue_model,
                "queue_name=s" => \$queue_name,
                "queue_parallel_environment=s" => \$queue_parallel_environment,
                "queue_command=s" => \$queue_command,
                "queue_nslots=i" => \$queue_nslots,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for BLAST contaminants\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$queue_file_name);
my (%names,%new_names,%short_names);

if (! -d $blast_folder){system ("mkdir $blast_folder");}

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Launch blast against gis of possible contaminants
print "Launch blast against possible contaminants\n";
$z=0;
foreach $x (sort { lc($a) cmp lc($b)} keys %short_names){
	$queue_file_name="$submission_folder/blast_$out_file\_$short_names{$x}\.sh";

	$z++;print "\tDoing $x ($z out of $sp_num) (BLAST contaminants in cluster)\n";

	system ("cp submission_queue_model.txt $queue_file_name");
	system ("sed -i 's/JOBNAME/BC_$x/' $queue_file_name");
	system ("sed -i 's/QUEUE/$queue_name/' $queue_file_name");
	system ("sed -i 's/ENVIRONMENT/$queue_parallel_environment/' $queue_file_name");
	system ("sed -i 's/NSLOTS/$queue_nslots/' $queue_file_name");

	if ($gis_file ne "none"){
		$y="blastn -db $nt_file -gilist $gis_file -query $transdecoder_folder/$short_names{$x}.longest_orfs.cds -out $blast_folder/$x.$out_file.blastn.out -num_threads $queue_nslots -dust yes -evalue $contaminant_evalue -max_target_seqs $contaminant_max_target_seqs -outfmt 6";
	}
	else {
        $y="blastn -db $nt_file -query $transdecoder_folder/$short_names{$x}.longest_orfs.cds -out $blast_folder/$x.$out_file.blastn.out -num_threads $queue_nslots -dust yes -evalue $contaminant_evalue -max_target_seqs $contaminant_max_target_seqs -outfmt 6";
	}


	system ("echo '$y' >> $queue_file_name");

	system ("$queue_command $queue_file_name");
}
