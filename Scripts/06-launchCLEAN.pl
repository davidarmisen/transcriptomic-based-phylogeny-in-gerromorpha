#!/usr/bin/perl
use strict;
use Getopt::Long;
require "./subroutines.txt";

# Create user defined variables
my ($hits_evalues,$hits_max_target_seqs,$transdecoder_folder,$transdecoder_filtered_folder,$blast_folder,$blast_filtered_folder,$submission_folder,$phylogeny_names,$identity,$coverage,$submission_queue_model,$queue_name,$queue_parallel_environment,$queue_nslots,$queue_command,$usegroups);

# Read user defined variables
GetOptions (  	"hits_evalues=f" => \$hits_evalues,
                "hits_max_target_seqs=f" => \$hits_max_target_seqs,
				"transdecoder_folder=s" => \$transdecoder_folder,
				"transdecoder_filtered_folder=s" => \$transdecoder_filtered_folder,
				"blast_folder=s" => \$blast_folder,
				"blast_filtered_folder=s" => \$blast_filtered_folder,
				"submission_folder=s" => \$submission_folder,
                "phylogeny_names=s" => \$phylogeny_names,
				"identity=i" => \$identity,
				"coverage=s" => \$coverage,
				"submission_queue_model=s" => \$submission_queue_model,
                "queue_name=s" => \$queue_name,
                "queue_parallel_environment=s" => \$queue_parallel_environment,
                "queue_command=s" => \$queue_command,
                "queue_nslots=i" => \$queue_nslots,
                "usegroups=i" => \$usegroups)
or die ("Error in command line arguments for BLAST\n");

# Create script variables
my ($sp_num,$x,$y,$z,$xx,$yy,$queue_file_name);
my (%names,%new_names,%short_names,%cont,$size,@a,@b);

if (! -d $blast_filtered_folder){system ("mkdir $blast_filtered_folder");}
if (! -d $transdecoder_filtered_folder){system ("mkdir $transdecoder_filtered_folder");}

# Load names
($sp_num,$x,$y,$z)=&Names($phylogeny_names,$usegroups);
%names=%$x;
%new_names=%$y;
%short_names=%$z;

# Use blastn contaminants results to filter blastp resuls and concatenate them. Concatenated file will be used by SiLiX on next step
print "Filter out contaminants from BLAST results and concatenate them\n";

# Check first that all files exists to save time if one is missing at the end so we don't need to repeat everything
$z=0;
print "Checking all files exists first\n";
foreach $x (sort { lc($a) cmp lc($b)} keys %names){
	$y=$new_names{$x};

    $z++;print "\tCheking $y ($z out of $sp_num) (Check files used to filter out contaminants)\n";
	
	# Check transdecoder files
    if (! -e "$transdecoder_folder/$x.longest_orfs.cds"){print "$transdecoder_folder/$x.longest_orfs.cds doesn't exist\n";exit(0);}
    if (! -e "$transdecoder_folder/$x.longest_orfs.pep.cd_hit.fa"){print "$transdecoder_folder/$x.longest_orfs.pep.cd_hit.fa doesn't exist\n";exit(0);}

	# Check contaminants files
	if (! -e "$blast_folder/$y.contaminant.blastn.out"){print "$blast_folder/$y.contaminant.blastn.out doesn't exist\n";exit(0);}
	# Check spA vs spB files
	foreach $xx (sort { lc($a) cmp lc($b)} keys %names){
		$yy=$new_names{$xx};
		if ($y ne $yy){
			if (! -e "$blast_folder/$y.$yy.blastp.out"){print "$blast_folder/$y.$yy.blastp.out doesn't exist\n";exit(0);}
		}
	}
}



# Launch filter per species
$z=0;
foreach $x (sort { lc($a) cmp lc($b)} keys %names){
	$y=$new_names{$x};

    $z++;

	open (SCRIPT,">$y\_filter_contaminants.pl");
	print SCRIPT "#!/usr/bin/perl\nuse strict;my (\%size,\%cont,\@a,\@b);\n";

	#print SCRIPT "print \"Loading transcripts size\\n\";\n";
	foreach $xx (sort { lc($a) cmp lc($b)} keys %names){
	    $yy=$new_names{$xx};

	    #$z++;print "\tCheking $y ($z out of $sp_num) (Loading transcripts size)\n";

		# Check contaminants files
		#print SCRIPT "
	    if (! -e "$transdecoder_folder/$xx.longest_orfs.cds"){print "$transdecoder_folder/$xx.longest_orfs.cds doesn't exist\n";exit(0);}
		print SCRIPT "
		\@a=`awk 'BEGIN{FS=\" \"}{print \\\$1}' $transdecoder_folder/$xx.longest_orfs.cds | sed -e 's/\\\\(^>.*\\\$\\\\)/#\\\\1|/' | tr -d \"\\\\r\" | tr -d \"\\\\n\" | sed -e 's/ //g'  | sed -e 's/\\\$/#/' | tr \"|\" \"\\\\t\" | tr \"#\" \"\\\\n\"  | sed -e '/^\\\$/d' | sed -e 's/>//' | awk '{print \\\$1,\"\\t\",length(\\\$2)}'`;
	    push \@b , \@a;
		";
		
	}
	print SCRIPT "
	foreach (\@b){chomp \$_;\@_=split(/\\\s+/,\$_);\$size{\$_[0]}=\$_[1];}
	";	

	# Check if file exists
	if (-e "$blast_folder/$y.filtered.contaminants.blastp.out"){system ("rm $blast_folder/$y.filtered.contaminants.blastp.out")}
	

	# Load contaminants
	foreach $xx (sort { lc($a) cmp lc($b)} keys %names){
		$yy=$new_names{$xx};

		if (! -e "$blast_folder/$yy.contaminant.blastn.out"){print "$blast_folder/$yy.contaminant.blastn.out doesn't exist\n";exit(0);}
		print SCRIPT '
open (OPEN,"<',$blast_folder,'/',$yy,'.contaminant.blastn.out");
while (<OPEN>){
    chomp $_;
	@_=split(/\t/,$_);
	if ($_[2]>=',$identity,' && ($_[3]/$size{$_[0]})>',$coverage,'){$cont{$_[0]}=1;} # 
}
close (OPEN);
		';
	}

	# Filter out contamintants and write results in a file per species
	if (! -e "$transdecoder_folder/$x.longest_orfs.cds"){print "$transdecoder_filtered_folder/$x.longest_orfs.cds doesn't exist\n";exit(0);}
	if (! -e "$transdecoder_folder/$x.longest_orfs.pep.cd_hit.fa"){print "$transdecoder_filtered_folder/$x.longest_orfs.pep.cd_hit.fa doesn't exist\n";exit(0);}

	print SCRIPT "
	\@a=();
        \@a=`awk 'BEGIN{FS=\" \"}{print \\\$1}' $transdecoder_folder/$x.longest_orfs.cds | sed -e 's/\\\\(^>.*\\\$\\\\)/#\\\\1|/' | tr -d \"\\\\r\" | tr -d \"\\\\n\" | sed -e 's/ //g'  | sed -e 's/\\\$/#/' | tr \"|\" \"\\\\t\" | tr \"#\" \"\\\\n\"  | sed -e '/^\\\$/d' | sed -e 's/>//' | awk '{print \\\$1,\"\\t\",\\\$2}'`;
        open (SAVE,\">$transdecoder_filtered_folder/$x.longest_orfs.filtered.cds\");
        foreach (\@a){
                chomp \$_;
                \@_=split(/\\\s+/,\$_);
                if (!\$cont{\$_[0]}){
                        print SAVE \">\$_[0]\\n\$_[1]\\n\";
                }
        }
        close (SAVE);
        \@a=();
        \@a=`awk 'BEGIN{FS=\" \"}{print \\\$1}' $transdecoder_folder/$x.longest_orfs.pep.cd_hit.fa | sed -e 's/\\\\(^>.*\\\$\\\\)/#\\\\1|/' | tr -d \"\\\\r\" | tr -d \"\\\\n\" | sed -e 's/ //g'  | sed -e 's/\\\$/#/' | tr \"|\" \"\\\\t\" | tr \"#\" \"\\\\n\"  | sed -e '/^\\\$/d' | sed -e 's/>//' | awk '{print \\\$1,\"\\t\",\\\$2}'`;
        open (SAVE,\">$transdecoder_filtered_folder/$x.longest_orfs.filtered.pep.cd_hit.fa\");
        foreach (\@a){
                chomp \$_;
                \@_=split(/\\\s+/,\$_);
                if (!\$cont{\$_[0]}){
                        print SAVE \">\$_[0]\\n\$_[1]\\n\";
                }
        }
        close (SAVE);
";
	
	print SCRIPT "
	open (SAVE,\">$blast_filtered_folder/$y.filtered.contaminants.blastp.out\");
	";

	foreach $xx (sort { lc($a) cmp lc($b)} keys %names){
		$yy=$new_names{$xx};

		if ($y ne $yy){
        	#$z++;print "\tDoing $y - $yy ($z out of ",$sp_num*($sp_num-1),") (Filter out contaminants)\n";
			if (! -e "$blast_folder/$y.$yy.blastp.out"){print "$blast_folder/$y.$yy.blastp.out doesn't exist\n";exit(0);}
			print SCRIPT '
open (SAVE,">>',$blast_filtered_folder,'/',$y,'.filtered.contaminants.blastp.out");
open (SAVE2,">',$blast_filtered_folder,'/',$y,'.',$yy,'.filtered.contaminants.blastp.out");
open (OPEN,"<',$blast_folder,'/',$y,'.',$yy,'.blastp.out");
while (<OPEN>){
        chomp $_;
	@_=split(/\t/,$_);
	if (!$cont{$_[0]} && !$cont{$_[1]}){print SAVE "$_\n";print SAVE2 "$_\n";}
}
close (OPEN);
close (SAVE);
close (SAVE2);
			';
		}
	}

	close (SCRIPT);

	$queue_file_name="$submission_folder/clean_$y\.sh";
	system ("cp submission_queue_model.txt $queue_file_name");
	system ("sed -i 's/JOBNAME/LC_$y/' $queue_file_name");
	system ("sed -i 's/QUEUE/$queue_name/' $queue_file_name");
	system ("sed -i 's/ENVIRONMENT/$queue_parallel_environment/' $queue_file_name");
	system ("sed -i 's/NSLOTS/$queue_nslots/' $queue_file_name");

	system ("chmod +x $y\_filter_contaminants.pl");
	system ("echo './$y\_filter_contaminants.pl' >> $queue_file_name");
	print "\tLaunching $y ($z out of $sp_num) (Filter out contaminants)\n";
	system ("$queue_command $queue_file_name");
}
